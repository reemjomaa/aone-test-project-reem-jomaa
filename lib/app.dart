
import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/resources/localization.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/translation/app_translation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'core/resources/defaults.dart';
import 'core/style/theme/themes.dart';
import 'features/splash/splash_screen.dart';



final navigatorKey = GlobalKey<NavigatorState>();

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


   @override
  Widget build(BuildContext context) {
    final AppController appController = Get.put(AppController());
    Get.put(DataController());
    Default.preferredOrientation();
    return ScreenUtilInit(
        designSize: const Size(395, 900),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context,child) {
          return GetMaterialApp(
            title: 'Aone',
            navigatorKey: navigatorKey,
            debugShowCheckedModeBanner: false,
            theme: AppTheme.Dark.getTheme,

            ///Localization
            translations: AppTranslation(),
            localizationsDelegates: const [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            locale: appController.locale == AppLocalization.Ar
                ? const Locale('ar', 'AE')
                : const Locale('en', 'UK'),
            initialRoute: '/',
            unknownRoute: AppRouting.unknownRoute,
            getPages: AppRouting.routes(),
            home: SplashScreen(),
          );
        }
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child,
      ScrollableDetails details) {
    return child;
  }
}

