import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/features/auth/presentaion/widgets/auth_background_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

class OnBoardingPage extends StatelessWidget {
  const OnBoardingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthBackgroundWidget(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: 80.h,),
          WidgetAnimator(
            incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
              duration: const Duration(seconds: 1),
            ),
            child: Assets.global.appLogo.image(
                width: 140.w,
                height: 70.h
            ),
          ),
          SizedBox(height: 35.h,),
          WidgetAnimator(
            incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
              duration: const Duration(seconds: 1),
            ),
            child: Text(
              'Future Aviation Forum',
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(fontWeight: AppFontWeight.semiBold),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 15.h,),
          TextAnimator(
            'Make a good first impression',
            style: Theme.of(context).textTheme.titleLarge,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 15,),
          Text(
            'Log in create your profile and get the most out of your event experience',
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(color: AppColors.whiteOpacityColor(0.4)),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 45.h,),
          AppButton(
              text: 'Sign In',
              withLoading: false,
              margin: EdgeInsets.zero,
              onTap: ()async=> Nav.replacement(Pages.login)
          ),
          SizedBox(height: 30.h,),
          InkWell(
            onTap: ()=> Nav.replacement(Pages.signUp),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Register For The Event?',
                style: Theme.of(context).textTheme.bodySmall,
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(height: 60.h,),
        ],
      ),
    );
  }
}
