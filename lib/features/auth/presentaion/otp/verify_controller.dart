import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/repositories/auth_repository.dart';
import 'package:aone/core/resources/api.dart';
import 'package:aone/core/resources/enums.dart';
import 'package:aone/core/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class VerifyController extends GetxController {
  DataController dataController = Get.find();
  AppController appController = Get.find();

  String email = '';
  TextEditingController pinCode = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();


  final RxString _time = ''.obs;
  String get time => _time.value;
  set time(value) => _time.value = value;

  final Rx<bool> _resendLoading = false.obs;
  bool get resendLoading => _resendLoading.value;
  set resendLoading(bool value) => _resendLoading.value = value;


  bool _timerBreaker = false;
  timerBreaker() async {
    _timerBreaker = true;
    await Future.delayed(1.seconds);
    _timerBreaker = false;
  }


  timer(Duration d) async {
    time = d.toString().substring(2, 7);
    await timerBreaker();
    while (d.inSeconds > 0 && !_timerBreaker) {
      await Future.delayed(1.seconds);
      d = (d.inSeconds - 1).seconds;
      time = d.toString().substring(2, 7);
    }
    time = '';
  }

  verifyCode(context)async {
    if(formKey.currentState!.validate()){
      signInConfirmAccount(context);
    }else{
      Get.back();
    }
  }

  resendCode(context) async {
    if (time.isNotEmpty || resendLoading) return;
    resendLoading = true;
    timer(120.seconds);
    ResponseModel response = await AuthRepo.requestOtp('m@aone.sa');
    if (response.success) {
      resetField();
      appController.showToast(context, message: response.message!,status: ToastStatus.success);
    } else {
      appController.showToast(context, message: response.message!,status: ToastStatus.fail);
    }
    resendLoading = false;
  }

  ///TODO: please notice that I put the email fixed because there is no register api or login api in
  ///TODO: postman file that you provide it to me in the email task
  signInConfirmAccount(BuildContext context) async {
    try{
      ResponseModel response = await AuthRepo.verifyOtp('m@aone.sa',pinCode.text,extraParam: 'token');
      if(response.success){
        Get.back();
        appController.setToken(response.extra);
        appController.showToast(context, message: 'Login Successfully',status: ToastStatus.success);
        Nav.offAll(Pages.navBar);
      }else if (response.code == ErrorCode.VALIDATION_ERROR || response.errors != null || response.errors!.isNotEmpty){
        Get.back();
        appController.showToast(context, message: response.errors!.first,status: ToastStatus.fail);
      }else{
        Get.back();
        appController.showToast(context, message: response.message!,status: ToastStatus.fail);
      }
    }catch(e){
      Get.back();
    }
  }

  navigateToLogin(){
    Nav.offAll(Pages.login);
  }

  resetField(){
    pinCode.clear();
  }

  @override
  void onInit() {
    if (Get.arguments != null) {
      email = Get.arguments['email'];
    }
    timer(10.seconds);
    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }
}
