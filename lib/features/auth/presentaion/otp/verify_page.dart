import 'package:aone/core/helpers/string_helper.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/features/auth/presentaion/widgets/auth_background_widget.dart';
import 'package:aone/features/nav_bar_pages/nav_bar/nav_bar_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pinput/pinput.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import '../../../../core/helpers/validator.dart';
import 'verify_controller.dart';


class VerifyPage extends StatelessWidget {
  const VerifyPage({super.key});

  @override
  Widget build(BuildContext context) {
    VerifyController controller = Get.put(VerifyController());
    return AuthBackgroundWidget(
      child: Form(
        key: controller.formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 80.h,),
              WidgetAnimator(
                incomingEffect: WidgetTransitionEffects.incomingScaleUp(
                  duration: const Duration(seconds: 1),
                ),
                child: Text(
                  'Confirmation Code',
                  style: Theme.of(context).textTheme.headlineSmall,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30.h,),
              TextAnimator(
                'Please write the 5-digit code sent via ${controller.email} correctly',
                style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.light),
                textAlign: TextAlign.center,
              ),

              SizedBox(height: 40.h,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: [
                    Pinput(
                      controller: controller.pinCode,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      length: 5,
                      validator: (input)=> Validator.verifyCodeValidation(input!),
                      errorTextStyle: Theme.of(context).textTheme.bodySmall,
                      submittedPinTheme: PinTheme(
                        height: 38.w,
                        width: 43.h,
                        textStyle: Theme.of(context).textTheme.bodyLarge,
                        decoration: BoxDecoration(
                            color: AppColors.authBackgroundColor,
                            borderRadius: BorderRadius.circular(8.r),
                            border: Border.all(
                                color: AppColors.secondaryColor
                            )
                        ),
                      ),
                      defaultPinTheme: PinTheme(
                        height: 38.w,
                        width: 43.h,
                        textStyle: Theme.of(context).textTheme.titleLarge,
                        decoration: BoxDecoration(
                          color: AppColors.authBackgroundColor,
                          borderRadius: BorderRadius.circular(8.r),
                          border: Border.all(
                            color: AppColors.secondaryColor
                          )
                        ),
                      ),
                    ),
                    SizedBox(height: 20.h,),
                    Obx(
                        ()=> Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              controller.time.isEmpty?'00:00':controller.time,
                              style: Theme.of(context).textTheme.labelLarge!.copyWith(
                                  color: AppColors.secondaryColor
                              ),
                            ),

                            InkWell(
                              onTap: controller.time.isEmpty?()=> controller.resendCode(context):null,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(vertical: 8.0),
                                child: Text(
                                  'Resend The Code',
                                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                                      color: controller.time.isEmpty?
                                      AppColors.secondaryColor:
                                      AppColors.secondaryColor.withOpacity(0.7)
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 40.h,),
              AppButton(
                  text: 'Go',
                  withLoading: true,
                  margin: EdgeInsets.zero,
                  onTap: ()async=> controller.verifyCode(context)
              ),

              SizedBox(height: 100.h,),
            ],
          ),
        ),
      ),
    );
  }
}
