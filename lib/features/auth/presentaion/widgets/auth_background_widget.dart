import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/helpers/media_query_helper.dart';
import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AuthBackgroundWidget extends StatelessWidget {
  final Widget child;
  const AuthBackgroundWidget({required this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Assets.images.authBackground.image(
              width: context.screenWidth,
              height: context.screenHeight,
              fit: BoxFit.cover
          ),
          Container(
            color: AppColors.backgroundColor.withOpacity(0.75),
            width: context.screenWidth,
            height: context.screenHeight,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 35),
            decoration: BoxDecoration(
              color: AppColors.primaryColor,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(60.r),
                topRight: Radius.circular(60.r),
              )
            ),
            child: child,
          )
        ],
      ),
    );
  }
}
