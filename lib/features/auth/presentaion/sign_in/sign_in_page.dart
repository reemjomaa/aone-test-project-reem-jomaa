
import 'package:aone/core/helpers/string_helper.dart';
import 'package:aone/core/helpers/validator.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/core/widgets/app_text_feild.dart';
import 'package:aone/features/auth/presentaion/sign_in/sign_in_controller.dart';
import 'package:aone/features/auth/presentaion/sign_up/sign_up_page.dart';
import 'package:aone/features/nav_bar_pages/nav_bar/nav_bar_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';
import '../widgets/auth_background_widget.dart';
import 'widgets/remember_me_widget.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  @override
  Widget build(BuildContext context) {
    SignInController controller = Get.put(SignInController());
    return AuthBackgroundWidget(
      child: Form(
        key: controller.formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 80.h,),
              WidgetAnimator(
                incomingEffect: WidgetTransitionEffects.incomingScaleUp(
                  duration: const Duration(seconds: 1),
                ),
                child: Text(
                  'Sign In',
                  style: Theme.of(context).textTheme.headlineSmall,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 100.h,),
              Text(
                'Email',
                style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.light),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: 15.h,),
              AppTextField(
                controller.email,
                validator: Validator.emailValidation,
              ),
              SizedBox(height: 30.h,),
              const RememberMeWidget(),
              SizedBox(height: 30.h,),
              AppButton(
                  text: 'Go',
                  withLoading: true,
                  margin: EdgeInsets.zero,
                  onTap: ()async=> controller.signIn(context)
              ),
              SizedBox(height: 30.h,),
              InkWell(
                onTap: ()=> Nav.to(Pages.signUp),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Register For The Event?',
                    style: Theme.of(context).textTheme.bodySmall,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(height: 60.h,),
            ],
          ),
        ),
      ),
    );
  }
}
