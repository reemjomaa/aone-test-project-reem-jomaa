import 'package:aone/core/widgets/app_switcher_widget.dart';
import 'package:flutter/material.dart';

class RememberMeWidget extends StatelessWidget {
  const RememberMeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Remember me',
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        AppSwitcherWidget(
          isSelected: false,
          afterSwitch: (value){},
        ),
      ],
    );
  }
}
