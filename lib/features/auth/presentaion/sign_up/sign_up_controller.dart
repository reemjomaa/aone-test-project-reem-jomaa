import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/repositories/auth_repository.dart';
import 'package:aone/core/resources/enums.dart';
import 'package:aone/core/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpController extends GetxController {
  AppController appController = Get.find();
  DataController dataController = Get.find();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController username = TextEditingController();
  TextEditingController email = TextEditingController();


  ///TODO: please notice that I put the email fixed because there is no register api or login api in
  ///TODO: postman file that you provide it to me in the email task
  signUp(context) async {
    if (formKey.currentState!.validate()) {
      try{
        ResponseModel response = await AuthRepo.requestOtp('m@aone.sa');
        if (response.success) {
          Get.back();
          appController.showToast(context, message: response.message,status: ToastStatus.success);
          Nav.to(Pages.verify,arguments: {'email': email.text});
        } else if (response.code == ErrorCode.VALIDATION_ERROR || response.errors != null ||
            response.errors!.isNotEmpty){
          Get.back();
          appController.showToast(context, message: response.errors!.first,status: ToastStatus.fail);
        }else{
          Get.back();
          appController.showToast(context, message: response.message,status: ToastStatus.fail);
        }
      }catch(e){
        Get.back();
      }
    } else {
      Get.back();
    }
  }
}
