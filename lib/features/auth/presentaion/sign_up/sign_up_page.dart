import 'package:aone/core/helpers/validator.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/core/widgets/app_text_feild.dart';
import 'package:aone/features/auth/presentaion/widgets/auth_background_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import 'sign_up_controller.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  @override
  Widget build(BuildContext context) {
    SignUpController controller = Get.put(SignUpController());
    return AuthBackgroundWidget(
      child: Form(
        key: controller.formKey,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 80.h,),
              WidgetAnimator(
                incomingEffect: WidgetTransitionEffects.incomingScaleUp(
                  duration: const Duration(seconds: 1),
                ),
                child: Text(
                  'Sign Up',
                  style: Theme.of(context).textTheme.headlineSmall,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 100.h,),
              Text(
                'Full Name',
                style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.light),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: 15.h,),
              AppTextField(
                controller.username,
                validator: Validator.notNullValidation,
              ),
              SizedBox(height: 30.h,),
              Text(
                'Email',
                style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.light),
                textAlign: TextAlign.start,
              ),
              SizedBox(height: 15.h,),
              AppTextField(
                  controller.email,
                validator: Validator.emailValidation,
              ),
              SizedBox(height: 30.h,),
              AppButton(
                  text: 'Go',
                  withLoading: true,
                  margin: EdgeInsets.zero,
                  onTap: ()async=> controller.signUp(context)
              ),
              SizedBox(height: 30.h,),
              InkWell(
                onTap: ()=> Get.back(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Already have an account?',
                    style: Theme.of(context).textTheme.bodySmall,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(height: 60.h,),
            ],
          ),
        ),
      ),
    );
  }
}
