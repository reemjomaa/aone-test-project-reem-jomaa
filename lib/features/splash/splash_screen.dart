
import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/helpers/media_query_helper.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/style/gradients.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';
import 'splash_controller.dart';


class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(SplashScreenController());
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Assets.images.authBackground.image(
              width: context.screenWidth,
              height: context.screenHeight,
              fit: BoxFit.fill
          ),
          Container(
            width: context.screenWidth,
            height: context.screenHeight,
            decoration: BoxDecoration(
              gradient: AppGradient.getSplashGradient()
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40,right: 40,top: 150),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WidgetAnimator(
                    incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
                      duration: const Duration(seconds: 1),
                    ),
                    child: Assets.global.appLogo.image(
                        width: 230.w,
                        height: 115.h
                    ),
                  ),
                  SizedBox(
                    height: context.screenHeight/20,
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(end: context.screenWidth/6),
                    child: TextAnimator(
                      'Elevating Global Connectivity',
                      style: Theme.of(context).textTheme.headlineLarge,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Divider(
                    height: context.screenHeight/25,
                    endIndent: context.screenWidth/25,
                  ),
                  WidgetAnimator(
                    incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
                      duration: const Duration(seconds: 1),
                    ),
                    child: Text(
                      'WHEN & WHERE',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          color: AppColors.textPrimaryColor,
                          fontWeight: AppFontWeight.medium
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  WidgetAnimator(
                    incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
                      duration: const Duration(milliseconds: 1500),
                    ),
                    child: Text(
                      '20 - 22 May 2024',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  const SizedBox(
                    height: 6,
                  ),
                  WidgetAnimator(
                    incomingEffect: WidgetTransitionEffects.incomingSlideInFromBottom(
                      duration: const Duration(milliseconds: 1500),
                    ),
                    child: Text(
                      'Riyadh, Saudi Arabia',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
