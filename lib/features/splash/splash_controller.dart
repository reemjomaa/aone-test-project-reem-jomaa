import 'dart:developer';

import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/routes.dart';
import 'package:get/get.dart';

class SplashScreenController extends GetxController {
  AppController appController = Get.find();



  loadData() async {
    await appController.loadUserData();
    await 3.seconds.delay();
    if(appController.token.isNotEmpty){
      Nav.replacement(Pages.navBar);
    }else{
      Nav.replacement(Pages.onBoarding);
    }
    try{
      log('${appController.token}');
      log('${appController.role.toString()}');
    }catch(e){}
  }

  @override
  void onInit() {
    loadData();
    super.onInit();
  }
}
