import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavItem extends StatelessWidget {
  final bool isActive;
  final String icon;
  final VoidCallback onTap;

  const BottomNavItem({Key? key,required this.onTap,required this.icon, this.isActive = false,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) {
        return ScaleTransition(scale: animation, child: child);
      },
      duration: const Duration(milliseconds: 200),
      reverseDuration: const Duration(milliseconds: 100),
      child: InkWell(
        onTap: onTap,
        child: Container(
                width: 40,
                height: 40,
                decoration: isActive ? BoxDecoration(
                  color: AppColors.secondaryColor,
                  borderRadius: BorderRadius.circular(8.r)
                ):null,
                child: Center(
                  child: SizedBox(
                    height: 25,
                    width: 25,
                    child: SvgPicture.asset(
                      icon,
                      color: isActive
                          ? AppColors.primaryColor
                          : AppColors.whiteColor,
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
