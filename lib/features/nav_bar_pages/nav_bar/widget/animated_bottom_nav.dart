import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'bottom_nav_item.dart';

class AnimatedBottomNav extends StatelessWidget {
  final int? currentIndex;
  final Function(int)? onChange;

  AnimatedBottomNav({Key? key, this.currentIndex, this.onChange})
      : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      decoration: BoxDecoration(
        color: AppColors.primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(35.r),
          topRight: Radius.circular(35.r),
        )
      ),
      // padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 15),
      child: Row(
        children: [
          Expanded(
            child: BottomNavItem(
              icon: Assets.icons.homeIcon.path,
              onTap: () => onChange!(0),
              isActive: currentIndex == 0,
            ),
          ),
          Expanded(
            child: BottomNavItem(
              icon: Assets.icons.calenderIcon.path,
              onTap: () => onChange!(1),
              isActive: currentIndex == 1,
            ),
          ),
          Expanded(
            child: BottomNavItem(
              icon: Assets.icons.profileIcon.path,
              onTap: () => onChange!(2),
              isActive: currentIndex == 2,
            ),
          ),
          Expanded(
            child: BottomNavItem(
              icon: Assets.icons.moreIcon.path,
              onTap: () => onChange!(3),
              isActive: currentIndex == 3,
            ),
          ),
        ],
      ),
    );
  }
}
