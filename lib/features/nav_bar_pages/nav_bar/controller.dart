import 'dart:io';
import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/resources/api.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/widgets/loading.dart';
import 'package:flutter/services.dart';
import '../home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../more/more_page.dart';

class NavBarController extends GetxController {
  AppController appController = Get.find();
  DataController dataController = Get.find();

  var scaffoldKey = GlobalKey<ScaffoldState>();


  final Rx<int> _selectedIndex = 0.obs;
  int get selectedIndex => _selectedIndex.value;
  set selectedIndex(int value) => _selectedIndex.value = value;


  int taps = 0;

  Map<int, Widget> get pages => {
        0: HomePage(),
        1: Container(),
        2: Container(),
        3: const MorePage(),
    };

  bottomNavigationOnChange(BuildContext context,int index){
    selectedIndex = index;
    // if(index==2||index==3||index==4){
    //   // appController.showToast(context, message: 'Please Login First'.tr,status: ToastStatus.warning);
    // }else{
    //   selectedIndex = index;
    // }
  }



  logOut(context) async {
    Loading.overlayLoading(context);
    ResponseModel response;
    response = await dataController.getData(
      url: API.logout,
    );
    2.seconds.delay();
    Get.back();
    appController.removeUserData();
    Nav.offAll(Pages.login);
    // if(response.success){
    //   // globalController.updateBaseUrl(API.globalBaseUrl);
    //   Get.back();
    //   appController.removeUserData();
    //   appController.showToast(context, message: response.message,status: ToastStatus.success);
    //   Nav.offAll(Pages.login);
    // }else{
    //   Get.back();
    //   appController.showToast(context, message: response.message);
    // }
  }


  // deleteAccount(context)async {
  //   Loading.overlayLoading(context);
  //   ResponseModel response;
  //   response = await dataController.deleteData(
  //     url: API.deleteAccount,
  //   );
  //   // 2.seconds.delay();
  //   // Get.back();
  //   // appController.removeUserData();
  //   // Nav.offAll(Pages.login);
  //   if(response.success){
  //     Get.back();
  //     appController.removeUserData();
  //     appController.showToast(context, message: response.message,status: ToastStatus.success);
  //     Nav.offAll(Pages.login);
  //   }else{
  //     Get.back();
  //     appController.removeUserData();
  //     appController.showToast(context, message: response.message);
  //     Nav.offAll(Pages.login);
  //   }
  // }

  // bool isUserGuest(){
  //   if(appController.role==Role.guest) return true;
  //   return false;
  // }

  // closeApp(BuildContext context) {
  //   print('taps $taps');
  //   if (taps == 1) {
  //     print('in close if');
  //     if (Platform.isAndroid) {
  //       SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  //     } else if (Platform.isIOS) {
  //       exit(0);
  //     }
  //   }
  //   Fluttertoast.showToast(
  //     msg: 'Press twice to exist'.tr,
  //     toastLength: Toast.LENGTH_SHORT,
  //     fontSize: 12,
  //   );
  //   taps++;
  //
  //   Future.delayed(const Duration(milliseconds: 500), () {
  //     taps = 0;
  //   });
  // }


  @override
  void onInit() {
    super.onInit();
  }
}
