


import 'package:aone/features/nav_bar_pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller.dart';
import 'widget/animated_bottom_nav.dart';

class NavBarPage extends StatelessWidget {
  const NavBarPage({super.key});

  @override
  Widget build(BuildContext context) {
    NavBarController controller = Get.put(NavBarController());
    return PopScope(
      onPopInvoked: (value) {},
      // onPopInvoked: (value) => controller.closeApp(context),
      // onWillPop: ()async{
      //   // controller.closeApp(context);
      //   return false;
      // },
      child: Obx(
            () => Scaffold(
          key: controller.scaffoldKey,
          resizeToAvoidBottomInset: true,
          body: controller.pages[controller.selectedIndex]!,
          bottomNavigationBar: AnimatedBottomNav(
            currentIndex: controller.selectedIndex,
            onChange: (index) => controller.bottomNavigationOnChange(context,index),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        ),
      ),
    );
  }
}
