import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/helpers/media_query_helper.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/style/gradients.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

class HomeAppBarWidget extends StatelessWidget {
  const HomeAppBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Assets.images.homeAppBarPackground.image(
            width: context.screenWidth,
            height: context.screenHeight/2,
            fit: BoxFit.fill
        ),
        Container(
          width: context.screenWidth,
          height: context.screenHeight/2,
          decoration: BoxDecoration(
            gradient: AppGradient.getHomeAppBarGradient()
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: WidgetAnimator(
                incomingEffect: WidgetTransitionEffects.incomingSlideInFromTop(
                  duration: const Duration(seconds: 1),
                ),
                child: Assets.global.appLogo.image(
                    width: 175.w,
                    height: 90.h
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextAnimator(
              'Elevating Global Connectivity',
              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                fontWeight: AppFontWeight.black
              ),
              characterDelay: const Duration(milliseconds: 100),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ],
    );
  }
}
