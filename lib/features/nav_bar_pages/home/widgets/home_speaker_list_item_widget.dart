import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_image_widget.dart';
import 'package:aone/features/nav_bar_pages/speakers/models/speaker_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HomeSpeakerListItemWidget extends StatelessWidget {
  final SpeakerModel speaker;
  final bool isSelected;
  const HomeSpeakerListItemWidget({required this.speaker, required this.isSelected,super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 125,
      decoration: BoxDecoration(
        color: AppColors.primaryColor,
        borderRadius: BorderRadius.circular(15.r),
        border: isSelected?Border.all(color: AppColors.secondaryColor):null
      ),
      margin: const EdgeInsetsDirectional.only(end: 16),
      child: Column(
        children: [
          AppImageWidget(
            speaker.image,
            type: ImageType.cachedNetwork,
            width: 125,
            height: 145,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.r),
              topRight: Radius.circular(15.r),
            ),
            fit: BoxFit.cover,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  speaker.name,
                  style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      fontWeight: AppFontWeight.semiBold,
                      color: AppColors.secondaryColor
                  ),
                ),
                const SizedBox(
                  height: 6,
                ),
                Text(
                  speaker.sampleDescription,
                  style: Theme.of(context).textTheme.labelSmall,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
