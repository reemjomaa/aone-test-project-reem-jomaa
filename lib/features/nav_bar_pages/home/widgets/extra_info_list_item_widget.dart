import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/helpers/media_query_helper.dart';
import 'package:aone/core/helpers/url_launcher_helper.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/gradients.dart';
import 'package:aone/core/widgets/app_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'home_title_widget.dart';

class ExtraInfoListItemWidget extends StatelessWidget {
  final String title;
  final String subTitle;
  final VoidCallback onTap;
  const ExtraInfoListItemWidget({required this.title,required this.onTap,required this.subTitle,super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HomeTitleWidget(
          title: title,
        ),
        const SizedBox(
          height: 15,
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(15.r),
          child: InkWell(
            onTap: onTap,
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                AppImageWidget(
                  Assets.images.extraInfoBackground.path,
                  type: ImageType.asset,
                  height: 95,
                  width: context.screenWidth,
                  borderRadius: BorderRadius.circular(15.r),
                  fit: BoxFit.cover,
                ),
                Container(
                  width: context.screenWidth,
                  decoration: BoxDecoration(
                    gradient: AppGradient.getHomeExtraInfoGradient()
                  ),
                  padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 20),
                  child: Text(
                    subTitle,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
