import 'package:aone/core/gen/assets.gen.dart';
import 'package:flutter/material.dart';

import 'home_title_widget.dart';
import 'home_venue_info_widget.dart';

class HomeVenueSectionWidget extends StatelessWidget {
  const HomeVenueSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const HomeTitleWidget(
            title: 'Venue',
          ),
          const SizedBox(
            height: 10,
          ),
          HomeVenueInfoWidget(
            iconPath: Assets.icons.addressIcon.path,
            data: 'King Abdulaziz International Conference Center (KAICC), Al Hada, Riyadh 12912, Saudi Arabia',
          ),
          const SizedBox(
            height: 14,
          ),
          HomeVenueInfoWidget(
            iconPath: Assets.icons.timeIcon.path,
            data: 'Day 1: 09:00 - 18:00\nDay 2: 09:00 - 17:00\nDay 3: By invitation Only 08:30 - 14:00',
          ),
        ],
      ),
    );
  }
}
