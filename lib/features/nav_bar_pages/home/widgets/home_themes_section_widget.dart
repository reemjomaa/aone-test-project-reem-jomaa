import 'package:aone/core/style/colors.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import 'home_title_widget.dart';
import 'theme_list_item_widget.dart';

class HomeThemesSectionWidget extends StatelessWidget {
  const HomeThemesSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      child: Column(
        children: [
          const HomeTitleWidget(
            title: 'Themes',
          ),
          const SizedBox(
            height: 10,
          ),
          ListViewObserver(
            controller: controller.speakersObserverController,
            onObserve: (result){
              print(result.firstChild?.index);
              controller.onThemeObserve(result.firstChild!.index);
            },
            child: SizedBox(
              height: 110,
              child: ListView.builder(
                controller: controller.themesScrollController,
                itemCount: controller.themes.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return ThemeListItemWidget(
                    theme: controller.themes[index],
                  );
                },
              ),
            ),
          ),
          const SizedBox(height: 20,),
          Obx(() => AnimatedSmoothIndicator(
            activeIndex: controller.themesCurrentItemIndex.value,
            count: controller.themes.length,
            effect: const WormEffect(
              dotHeight: 8,
              dotWidth: 8,
              activeDotColor: AppColors.secondaryColor,
              dotColor: AppColors.authBackgroundColor,
            ),
          ))

        ],
      ),
    );
  }
}
