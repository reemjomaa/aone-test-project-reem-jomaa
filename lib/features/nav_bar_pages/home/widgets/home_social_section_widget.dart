import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/helpers/media_query_helper.dart';
import 'package:aone/core/helpers/url_launcher_helper.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeSocialSectionWidget extends StatelessWidget {
  const HomeSocialSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: context.screenWidth/5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          InkWell(
            onTap: ()=> UrlLauncherHelper.openLauncher(controller.eventDetails.youtube),
            child: Assets.icons.youtubeIcon.image(),
          ),
          InkWell(
            onTap: ()=> UrlLauncherHelper.openLauncher(controller.eventDetails.twitter),
            child: Assets.icons.xIcon.image(),
          ),
          InkWell(
            onTap: ()=> UrlLauncherHelper.openLauncher(controller.eventDetails.linkedin),
            child: Assets.icons.linkedInIcon.image(),
          ),
        ],
      ),
    );
  }
}
