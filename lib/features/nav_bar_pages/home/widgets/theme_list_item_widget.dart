import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../models/theme_model.dart';

class ThemeListItemWidget extends StatelessWidget {
  final ThemeModel theme;
  const ThemeListItemWidget({required this.theme,super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      decoration: BoxDecoration(
        color: AppColors.primaryColor,
        borderRadius: BorderRadius.circular(125.r)
      ),
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 15),
      margin: const EdgeInsetsDirectional.only(end: 10),
      child: Row(
        children: [
          Container(
            width: 78,
            height: 78,
            decoration: const BoxDecoration(
              color: AppColors.secondaryColor,
              shape: BoxShape.circle
            ),
            child: Center(
              child: AppImageWidget(
                theme.logo,
                type: ImageType.cachedNetwork,
                width: 75,
                height: 75,
                borderRadius: BorderRadius.circular(50),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(width: 10,),
          Expanded(
            child: Text(
              theme.title,
              style: Theme.of(context).textTheme.bodySmall!.copyWith(
                fontWeight: AppFontWeight.bold
              ),
              overflow: TextOverflow.ellipsis,
              maxLines: 3,
            ),
          )
        ],
      ),
    );
  }
}
