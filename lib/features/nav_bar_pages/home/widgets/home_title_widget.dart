import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';

class HomeTitleWidget extends StatelessWidget {
  final String title;
  const HomeTitleWidget({required this.title,super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 5,
          height: 5,
          decoration: const BoxDecoration(
            color: AppColors.textPrimaryColor,
            shape: BoxShape.circle
          ),
        ),
        const SizedBox(width: 5,),
        Text(
          title,
          style: Theme.of(context).textTheme.titleSmall!.copyWith(
            fontWeight: AppFontWeight.bold
          ),
        )
      ],
    );
  }
}
