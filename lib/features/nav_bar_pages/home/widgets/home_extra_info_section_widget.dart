import 'package:aone/core/helpers/url_launcher_helper.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:aone/features/nav_bar_pages/home/pdf_reader_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'extra_info_list_item_widget.dart';

class HomeExtraInfoSectionWidget extends StatelessWidget {
  const HomeExtraInfoSectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return ListView.separated(
      itemCount: controller.extraInfoTitles.length,
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      itemBuilder: (context,index){
        return ExtraInfoListItemWidget(
          title: controller.extraInfoTitles[index],
          subTitle: controller.extraInfoSubTitles[index],
          onTap: ()=> controller.extraInfoUrls[index].isNotEmpty?
          Get.to(PDFReaderPage(pdfURL: controller.extraInfoUrls[index], title: controller.extraInfoSubTitles[index])):
          controller.appController.showToast(Get.context!, message: 'This is no link'),
        );
      },
      separatorBuilder: (context,index){
        return const SizedBox(height: 25,);
      },
    );
  }
}
