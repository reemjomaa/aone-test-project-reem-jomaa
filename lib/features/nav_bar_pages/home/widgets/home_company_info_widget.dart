
import 'package:aone/core/helpers/string_helper.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/animated_number_widget.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeCompanyInfoWidget extends StatelessWidget {
  const HomeCompanyInfoWidget({super.key});

  Widget infoWidget(context,{required int number,required String title,bool? withPlus}){
    return Column(
      children: [
        Row(
          children: [
            AnimatedNumberWidget(
              number: number,
            ),
            Visibility(
              visible: withPlus??false,
              child: Text(
                '+',
                style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                    fontWeight: AppFontWeight.semiBold,
                    color: AppColors.textPrimaryColor
                ),
              ),
            ),
          ],
        ),
        const SizedBox(height: 4,),
        Text(
          title,
          style: Theme.of(context).textTheme.labelLarge,
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return Container(
      color: AppColors.authBackgroundColor,
      padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          infoWidget(context,number: controller.eventDetails.expectedVisitors.getNumberFromString(), title: 'VISITORS EXPECTED',withPlus: controller.eventDetails.expectedVisitors.isNumberHasPlus()),
          infoWidget(context,number: controller.eventDetails.speakers.getNumberFromString(), title: 'SPEAKERS',withPlus: controller.eventDetails.speakers.isNumberHasPlus()),
          infoWidget(context,number: controller.eventDetails.countries.getNumberFromString(), title: 'COUNTRIES',withPlus: controller.eventDetails.countries.isNumberHasPlus()),
          infoWidget(context,number: controller.eventDetails.parteners.getNumberFromString(), title: 'PARTNERS',withPlus: controller.eventDetails.parteners.isNumberHasPlus()),
        ],
      ),
    );
  }
}
