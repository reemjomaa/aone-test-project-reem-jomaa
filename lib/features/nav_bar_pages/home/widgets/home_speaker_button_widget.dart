import 'package:aone/core/helpers/string_helper.dart';
import 'package:aone/core/routes.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/features/nav_bar_pages/speakers/speakers_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeSpeakerButtonWidget extends StatelessWidget {
  const HomeSpeakerButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Expanded(
          child: Divider(
            color: AppColors.secondaryColor,
          ),
        ),
        AppButton(
          text: 'View All Speakers',
          onTap: ()async=> Nav.to(Pages.speakers),
          margin: EdgeInsets.zero,
          fillColor: AppColors.backgroundColor,
          style: Theme.of(context).textTheme.bodySmall,
          borderColor: AppColors.secondaryColor,
          radius: 50,
          padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 12),
          withLoading: false,
        ),
        const Expanded(
          child: Divider(
            color: AppColors.secondaryColor,
          ),
        ),
      ],
    );
  }
}
