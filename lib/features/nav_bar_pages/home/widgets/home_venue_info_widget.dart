import 'package:aone/core/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeVenueInfoWidget extends StatelessWidget {
  final String iconPath;
  final String data;
  const HomeVenueInfoWidget({required this.data,required this.iconPath,super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(
          iconPath
        ),
        const SizedBox(width: 10,),
        Expanded(
          child: Text(
            data,
            style: Theme.of(context).textTheme.bodySmall!.copyWith(
              height: 1.5
            ),
          ),
        )
      ],
    );
  }
}
