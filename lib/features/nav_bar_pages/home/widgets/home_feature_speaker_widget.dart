import 'package:aone/core/style/colors.dart';
import 'package:aone/core/widgets/custom_page_view/dynamic_page_view.dart';
import 'package:aone/core/widgets/skeleton_widget.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'home_speaker_list_item_widget.dart';
import 'home_title_widget.dart';


class HomeFeatureSpeakerWidget extends StatelessWidget {
  const HomeFeatureSpeakerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const HomeTitleWidget(
            title: 'Features Speaker',
          ),
          const SizedBox(
            height: 10,
          ),
          Obx(
              ()=> controller.speakersLoading?
              SkeletonWidget(
                width: 125,
                height: 175,
                radius: 15,
                margin: const EdgeInsetsDirectional.only(end: 10),
              ) :Column(
                children: [
                  ListViewObserver(
                    controller: controller.speakersObserverController,
                    onObserve: (result){
                      print(result.firstChild?.index);
                      controller.onSpeakerObserve(result.firstChild!.index);
                    },
                    child: SizedBox(
                      height: 220,
                      child: ListView.builder(
                        controller: controller.speakersScrollController,
                        itemCount: controller.speakers.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Obx(
                              ()=> HomeSpeakerListItemWidget(
                                isSelected: controller.speakersCurrentItemIndex.value==index,
                                speaker: controller.speakers[index],
                              )
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(height: 20,),
                  Obx(() => AnimatedSmoothIndicator(
                    activeIndex: controller.speakersCurrentItemIndex.value,
                    count: controller.speakers.length,
                    effect: const WormEffect(
                      dotHeight: 8,
                      dotWidth: 8,
                      activeDotColor: AppColors.secondaryColor,
                      dotColor: AppColors.authBackgroundColor,
                    ),
                  ))

                ],
              )
          ),

        ],
      ),
    );
  }
}
