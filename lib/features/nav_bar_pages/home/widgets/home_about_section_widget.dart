
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'home_title_widget.dart';

class HomeAboutSectionWidget extends StatelessWidget {
  const HomeAboutSectionWidget({super.key});


  @override
  Widget build(BuildContext context) {
    ValueNotifier<bool> isExpanded = ValueNotifier<bool>(false);
    HomeController controller = Get.find();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          HomeTitleWidget(
            title: 'Discover the Future of Aviation',
          ),
          const SizedBox(
            height: 10,
          ),
          Visibility(
            visible:  controller.eventDetails.fullDescription.isNotEmpty,
            child: ValueListenableBuilder(
                valueListenable: isExpanded,
                builder: (context, bool expanded, _) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Visibility (
                        visible: controller.eventDetails.fullDescription.isNotEmpty,
                        child: Text(
                          controller.eventDetails.fullDescription.length > 150
                                ? expanded
                                ? controller.eventDetails.fullDescription
                                : '${controller.eventDetails.fullDescription.substring(0, 150)}...'
                                : controller.eventDetails.fullDescription,
                          style: Theme.of(context).textTheme.bodySmall!.copyWith(
                            fontWeight: AppFontWeight.regular
                          ),
                        ),
                      ),
                      const SizedBox(height: 6,),
                      Visibility(
                        visible: controller.eventDetails.fullDescription.length > 100,
                        child: InkWell(
                          onTap: () {
                            isExpanded.value = !isExpanded.value;
                          },
                          child: Text(
                            expanded
                                ? 'see less'
                                : 'see more',
                            style:Theme.of(context).textTheme.bodySmall!.copyWith(
                                fontWeight: AppFontWeight.bold,
                              color: AppColors.textPrimaryColor
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                }),
          )
        ],
      ),
    );
  }
}
