import 'package:aone/core/helpers/string_helper.dart';
import 'package:aone/core/widgets/map_widget.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class HomeCompanyLocationWidget extends StatelessWidget {
  const HomeCompanyLocationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    HomeController controller = Get.find();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: MapWidget(
        zooming: 10,
        height: 185,
        marker: Marker(
          markerId: MarkerId(0.toString()),
          position: controller.eventDetails.location.extractLatLngFromURl()
        ),
        startLocation: controller.eventDetails.location.extractLatLngFromURl()
      ),
    );
  }
}
