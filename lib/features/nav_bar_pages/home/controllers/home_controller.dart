
import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/resources/api.dart';
import 'package:aone/features/nav_bar_pages/home/models/event_details_model.dart';
import 'package:aone/features/nav_bar_pages/speakers/models/speaker_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';

import '../models/theme_model.dart';

class HomeController extends GetxController{
  AppController appController = Get.find();
  DataController dataController = Get.find();

  List<String> extraInfoTitles= ['Program at a glance','Sponsors and Partners','Sponsorships','Media Center'];
  List<String> extraInfoSubTitles= ['View Program','View Sponsors and Partners','PDF','View all Media'];
  List<String> extraInfoUrls= [];



  List<SpeakerModel> speakers= [];
  final RxBool _speakersLoading = true.obs;
  get speakersLoading  => _speakersLoading.value;
  set speakersLoading (value) => _speakersLoading.value = value;

  Future<void> loadSpeakersData() async {
    speakers=[];
    speakersLoading =true;
    ResponseModel response;
    response = await dataController.getData(
      url: API.getAllFeaturesSpeakers,
    );
    if(response.success){
      response.data.forEach((element) => speakers.add(SpeakerModel.fromJson(element)));
      speakersLoading =false;
    }
  }

  List<ThemeModel> themes= [];
  final RxBool _themesLoading = true.obs;
  get themesLoading  => _themesLoading.value;
  set themesLoading (value) => _themesLoading.value = value;

  Future<void> loadThemesData() async {
    themes=[];
    themesLoading =true;
    ResponseModel response;
    response = await dataController.getData(
      url: API.getThemes,
    );
    if(response.success){
      response.data.forEach((element) => themes.add(ThemeModel.fromJson(element)));
      themesLoading =false;
    }
  }


  late EventDetailsModel eventDetails;
  final RxBool _eventDetailsLoading = true.obs;
  get eventDetailsLoading => _eventDetailsLoading.value;
  set eventDetailsLoading (value) => _eventDetailsLoading.value = value;

  Future<void> loadEventDetailsData() async {
    eventDetailsLoading =true;
    ResponseModel response;
    response = await dataController.getData(
      url: API.getEventDetails,
    );
    if(response.success){
      eventDetails = EventDetailsModel.fromJson(response.data);
      extraInfoUrls.add(eventDetails.program);
      extraInfoUrls.add('');
      extraInfoUrls.add(eventDetails.sponsorship);
      extraInfoUrls.add('');
      eventDetailsLoading =false;
    }
  }

  final RxBool _dataLoading = true.obs;
  get dataLoading => _dataLoading.value;
  set dataLoading (value) => _dataLoading.value = value;

  loadPageData()async{
    dataLoading = true;
    await Future.wait(
      [loadEventDetailsData(),loadSpeakersData(),loadThemesData()] as Iterable<Future>
    );
    dataLoading = false;
  }


  ///speakers scroll controller///
  final ScrollController speakersScrollController = ScrollController();
  late ListObserverController speakersObserverController = ListObserverController(controller: speakersScrollController);
  final speakersPageController = PageController(initialPage: 0, keepPage: true);
  RxInt speakersCurrentItemIndex = 0.obs;

  onSpeakerObserve(int index){
    speakersCurrentItemIndex.value = index;
    speakersPageController.jumpToPage(index);
  }

  ///themes scroll controller///
  final ScrollController themesScrollController = ScrollController();
  late ListObserverController themesObserverController = ListObserverController(controller: themesScrollController);
  final themesPageController = PageController(initialPage: 0, keepPage: true);
  RxInt themesCurrentItemIndex = 0.obs;

  onThemeObserve(int index){
    themesCurrentItemIndex.value = index;
    themesPageController.jumpToPage(index);
  }

  @override
  void onInit() {
    loadPageData();
    super.onInit();
  }
}