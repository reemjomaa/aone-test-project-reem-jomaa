import 'package:aone/core/style/colors.dart';
import 'package:aone/core/widgets/skeleton_widget.dart';
import 'package:aone/features/nav_bar_pages/home/controllers/home_controller.dart';
import 'package:aone/features/nav_bar_pages/home/widgets/home_company_location_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'widgets/home_about_section_widget.dart';
import 'widgets/home_company_info_widget.dart';
import 'widgets/home_appBar_widget.dart';
import 'widgets/home_extra_info_section_widget.dart';
import 'widgets/home_feature_speaker_widget.dart';
import 'widgets/home_social_section_widget.dart';
import 'widgets/home_speaker_button_widget.dart';
import 'widgets/home_themes_section_widget.dart';
import 'widgets/home_venue_section_widget.dart';


class HomePage extends StatelessWidget {
  HomeController controller = Get.put(HomeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Obx(
            ()=> controller.dataLoading?
            const Center(
              child: CircularProgressIndicator(color: AppColors.secondaryColor,),
            ):
            ListView(
              children: const [
                HomeAppBarWidget(),
                HomeCompanyInfoWidget(),
                HomeAboutSectionWidget(),
                HomeThemesSectionWidget(),
                HomeFeatureSpeakerWidget(),
                HomeSpeakerButtonWidget(),
                SizedBox(height: 10,),
                HomeExtraInfoSectionWidget(),
                SizedBox(height: 25,),
                HomeVenueSectionWidget(),
                SizedBox(height: 25,),
                HomeCompanyLocationWidget(),
                SizedBox(height: 35,),
                HomeSocialSectionWidget(),
                SizedBox(height: 50,),
              ],
            )
        )
    );
  }
}
