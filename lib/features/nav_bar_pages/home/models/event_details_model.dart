class EventDetailsModel {
  String title;
  String sampleDescription;
  String fullDescription;
  String sponsorship;
  String program;
  String location;
  String instagram;
  String facebook;
  String twitter;
  String linkedin;
  String youtube;
  String googleplay;
  String polls;
  String privacy;
  String contactUs;
  String appstore;
  String speakers;
  String countries;
  String parteners;
  String expectedVisitors;

  EventDetailsModel(
      {required this.title,
        required this.sampleDescription,
        required this.fullDescription,
        required this.sponsorship,
        required this.program,
        required this.location,
        required this.instagram,
        required this.facebook,
        required this.twitter,
        required this.linkedin,
        required this.youtube,
        required this.googleplay,
        required this.polls,
        required this.privacy,
        required this.contactUs,
        required this.appstore,
        required this.speakers,
        required this.countries,
        required this.expectedVisitors,
        required this.parteners});

  factory EventDetailsModel.fromJson(Map<String, dynamic> json) => EventDetailsModel(
      title: json['title'],
      sampleDescription: json['sample_description'],
      fullDescription: json['full_description'],
      sponsorship: json['sponsorship'],
      program: json['program'],
      location: json['location'],
      instagram: json['instagram'],
      facebook: json['facebook'],
      twitter: json['twitter'],
      linkedin: json['linkedin'],
      youtube: json['youtube'],
      googleplay: json['googleplay'],
      polls: json['polls'],
      privacy: json['privacy'],
      contactUs: json['contact_us'],
      appstore: json['appstore'],
      speakers: json['static_data']['Speakers'],
      countries: json['static_data']['Countries'],
      parteners: json['static_data']['Parteners'],
      expectedVisitors: json['static_data']['Expected Visitors'],
  );

}
