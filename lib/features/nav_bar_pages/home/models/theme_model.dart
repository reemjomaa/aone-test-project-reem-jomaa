class ThemeModel {
  String logo;
  String title;
  String description;

  ThemeModel({required this.logo, required this.title, required this.description});

  factory ThemeModel.fromJson(Map<String, dynamic> json) => ThemeModel(
      logo: json['logo'],
      title: json['title'],
      description: json['description'],
  );


}
