import 'package:advance_pdf_viewer2/advance_pdf_viewer.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';

class PDFReaderPage extends StatefulWidget {
  final String pdfURL;
  final String title;
  PDFReaderPage({required this.pdfURL, required this.title});

  @override
  _PDFReaderPageState createState() => _PDFReaderPageState();
}

class _PDFReaderPageState extends State<PDFReaderPage> {
  late PDFDocument document;
  final ValueNotifier<bool> _isLoading = ValueNotifier<bool>(true);

  @override
  void initState() {
    loadPdf();
    super.initState();
  }

  void loadPdf() async {
    _isLoading.value = true;
    document = await PDFDocument.fromURL(widget.pdfURL);
    _isLoading.value = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: const IconThemeData(color: AppColors.secondaryColor),
          title: Text(
            widget.title,
            style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                color: AppColors.secondaryColor,
                fontWeight: AppFontWeight.semiBold),
          ),
          centerTitle: true,
        ),
        body: ValueListenableBuilder(
          valueListenable: _isLoading,
          builder: (context, bool loading, child) {
            return Center(
                child: loading
                    ? const Center(
                        child: CircularProgressIndicator(
                        color: AppColors.secondaryColor,
                      ))
                    : PDFViewer(
                        document: document,
                        zoomSteps: 1,
                        pickerButtonColor: AppColors.secondaryColor,
                        scrollDirection: Axis.vertical,
                        pickerIconColor: AppColors.whiteColor,
                        // tooltip: PDFViewerTooltip(),
                      ));
          },
        ));
  }
}
