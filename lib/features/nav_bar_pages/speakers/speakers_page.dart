import 'package:aone/core/resources/controllers_tags.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_appBar_widget.dart';
import 'package:aone/core/widgets/pagination/options/list_view.dart';
import 'package:aone/features/nav_bar_pages/speakers/models/speaker_model.dart';
import 'package:aone/features/nav_bar_pages/speakers/widgets/speaker_list_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import 'speakers_controller.dart';
import 'widgets/speaker_list_item_shimmer.dart';
import 'widgets/speakers_category_section_widget.dart';


class SpeakersPage extends StatelessWidget {
  const SpeakersPage({super.key});

  @override
  Widget build(BuildContext context) {
    SpeakersController controller = Get.put(SpeakersController());
    return Scaffold(
      body: Column(
        children: [
          AppAppbarWidget(
            centerWidget: Text(
              'All Speakers',
              style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  fontWeight: AppFontWeight.medium
              ),
            ),
            trailingIcon: const Icon(Icons.search),
            leadingIcon: const Icon(Icons.arrow_back_ios),
            trailingOnTap: () {},
            leadingOnTap: () {Get.back();},
          ),
          const SizedBox(height: 20,),
          const SpeakersCategorySectionWidget(),
          const SizedBox(height: 10,),
          Expanded(
            child: ListViewPagination<SpeakerModel>(
              tag: ControllersTags.speakersTag,
              fetchApi: controller.loadPurchasesData,
              fromJson: (json) => SpeakerModel.fromJson(json),
              initialLoading: ListView(
                padding: EdgeInsets.zero,
                children: [
                  for (int i = 0; i < 8; i++) const SpeakerListItemShimmer()
                ],
              ),
              loading: const SpeakerListItemShimmer(),
              padding: EdgeInsets.zero,
              itemBuilder: (context, index, length, speaker) => WidgetAnimator(
                incomingEffect: WidgetTransitionEffects.incomingSlideInFromRight(
                  duration: Duration(milliseconds: 200+((index%15)*50)),
                ),
                child: SpeakerListItemWidget(
                  speaker: speaker,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
