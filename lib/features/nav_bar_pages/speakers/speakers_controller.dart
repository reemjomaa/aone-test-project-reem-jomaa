
import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/resources/api.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';

class SpeakersController extends GetxController{
  DataController dataController = Get.find();
  AppController appController = Get.find();

  List<String> categories = ['Featured'];

  Future<ResponseModel> loadPurchasesData(int page, CancelToken cancel) async =>
      await dataController.getData(
        url: API.getAllSpeakers,
        param: {
          'page': page,
        },
        cancel: cancel,
      );


  @override
  void onInit() async {
    super.onInit();
  }
}