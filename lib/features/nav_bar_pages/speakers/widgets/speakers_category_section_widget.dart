import 'package:aone/features/nav_bar_pages/speakers/speakers_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'speakers_category_list_item_widget.dart';

class SpeakersCategorySectionWidget extends StatelessWidget {
  const SpeakersCategorySectionWidget({super.key});

  @override
  Widget build(BuildContext context) {
    SpeakersController controller = Get.find();
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          for(int i=0; i<controller.categories.length; i++)
            SpeakersCategoryFistItemWidget(title: controller.categories[i],)
        ],
      ),
    );
  }
}
