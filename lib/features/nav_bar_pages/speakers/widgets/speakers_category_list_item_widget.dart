import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';

class SpeakersCategoryFistItemWidget extends StatelessWidget {
  final String title;
  const SpeakersCategoryFistItemWidget({required this.title, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: AppColors.secondaryColor)
      ),
      child: Text(
        title,
        style: Theme.of(context).textTheme.bodyLarge!.copyWith(
          fontWeight: AppFontWeight.bold
        ),
      ),
    );
  }
}
