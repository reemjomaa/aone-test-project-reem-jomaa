import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_image_widget.dart';
import 'package:aone/features/nav_bar_pages/speakers/models/speaker_model.dart';
import 'package:flutter/material.dart';

class SpeakerListItemWidget extends StatelessWidget {
  final SpeakerModel speaker;
  const SpeakerListItemWidget({required this.speaker,super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 12),
      child: Row(
        children: [
          AppImageWidget(
            speaker.image,
            type: ImageType.cachedNetwork,
            width: 60,
            height: 60,
            borderRadius: BorderRadius.circular(50),
          ),
          const SizedBox(width: 15,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  speaker.name,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                    fontWeight: AppFontWeight.semiBold
                  ),
                ),
                const SizedBox(height: 10,),
                Text(
                  speaker.sampleDescription,
                  style: Theme.of(context).textTheme.bodySmall!.copyWith(
                    fontWeight: AppFontWeight.regular
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
