import 'package:aone/core/widgets/skeleton_widget.dart';
import 'package:flutter/material.dart';

class SpeakerListItemShimmer extends StatelessWidget {
  const SpeakerListItemShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SkeletonWidget(
            width: 60,
            height: 60,
            isCircular: true,
          ),
          const SizedBox(width: 15,),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SkeletonWidget(
                  width: 150,
                  height: 12,
                  radius: 0,
                ),
                const SizedBox(height: 16),
                SkeletonWidget(
                  height: 10,
                  radius: 0,
                ),
                const SizedBox(height: 6),
                SkeletonWidget(
                  height: 10,
                  radius: 0,
                ),
                const SizedBox(height: 6),
                SkeletonWidget(
                  height: 10,
                  radius: 0,
                ),
              ],
            ),
          ),

        ],
      ),
    );
  }
}
