
class SpeakerModel {
  String name;
  String sampleDescription;
  String fullDescription;
  String image;

  SpeakerModel(
      {required this.name,
      required this.sampleDescription,
      required this.fullDescription,
      required this.image});

  factory SpeakerModel.fromJson(Map<String, dynamic> json) => SpeakerModel(
      name : json['name'],
      sampleDescription : json['sample_description'],
      fullDescription : json['full_description'],
      image : json['image']
  );

}
