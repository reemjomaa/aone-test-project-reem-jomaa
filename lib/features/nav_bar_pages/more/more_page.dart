import 'package:aone/core/widgets/app_button.dart';
import 'package:aone/features/nav_bar_pages/nav_bar/controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MorePage extends StatelessWidget {
  const MorePage({super.key});

  @override
  Widget build(BuildContext context) {
    NavBarController controller = Get.find();
    return Center(
      child: AppButton(
          text: 'Log Out',
          onTap: ()=> controller.logOut(context),
          height: 60,
          width: 150,
      ),
    );
  }
}
