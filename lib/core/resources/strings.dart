import 'streams_variable.dart';

class Strings{


  // String getFieldRequired(){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return 'هذا الحقل مطلوب';
  //   }else{
  //     return 'This field is required';
  //   }
  // }
  //
  // String allString(){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return 'الكل';
  //   }else{
  //     return 'All';
  //   }
  // }

  // String dhlErrorHappen(){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return 'هذا العنوان غير مدعوم';
  //   }else{
  //     return 'This address is not supported';
  //   }
  // }

  // String emailNotValid(){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return 'هذا الايميل غير صالح';
  //   }else{
  //     return 'This email is not valid';
  //   }
  // }
  //
  // String fieldLessThanCharacters(int minLength){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return "هذا الحقل لا يمكن أن يكون أقل من ${minLength} محارف ";
  //   }else{
  //     return 'This field cannot be less than ${minLength} characters';
  //   }
  // }
  //
  // String fieldMoreThanCharacters(int minLength){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return "هذا الحقل لا يمكن أن يكون أكثر من ${minLength} محارف ";
  //   }else{
  //     return 'This field cannot be more than ${minLength} characters';
  //   }
  // }
  //
  // String fieldMustContainNumbers(){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return "هذا الحقل يجب أن يحوي على أرقام فقط";
  //   }else{
  //     return 'This field must contain numbers only';
  //   }
  // }
  //
  // String passwordLessThanCharacters(int minLength){
  //   if(globalSetting.value.mobileLanguage.value =='ar'){
  //     return "كلمة المرور لايجب أن تكون أقل من ${minLength} محارف";
  //   }else{
  //     return 'Password must not be less than ${minLength} characters';
  //   }
  // }
  //
  String anErrorHappen(){
    return 'Something went wrong, please try again';
  }

  String internetConnectionError(){
    return 'Something went wrong with the internet ,try again later';
  }


  String serverError(){
    return 'An error occurred within the server, please try again later';
  }



  String ForbiddenErrorMessage(){
    return 'Please Login first';
  }

  String NotFoundErrorMessage(){
    return 'Content not available';
  }

  String UnknownErrorMessage(){
    return 'An unknown error occurred';
  }

}