// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';
//
//
// class UrlHelper {
//   final urlRegExp = new RegExp(
//       r"((https?:www\.)|(https?:\/\/)|(www\.))[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9]{1,6}(\/[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)?");
//
//   List<UrlPosition> positions = [];
//
//   List<TextSpan> getAllUrlsFromText(String text) {
//     final urlMatches = urlRegExp.allMatches(text);
//     positions = urlMatches
//         .map((urlMatch) => UrlPosition(start: urlMatch.start,end: urlMatch.end)).toList();
//
//     var spans = <TextSpan>[];
//
//     if (positions.isEmpty) {
//       spans.add(TextSpan(text: text));
//     } else {
//       for (var i = 0; i < positions.length; i++) {
//         final strStart = i == 0 ? 0 : positions[i - 1].end;
//         spans.add(
//           TextSpan(
//             text: text.substring(
//               strStart,
//               positions[i].start,
//             ),
//           ),
//         );
//         spans.add(
//           TextSpan(
//             text: text.substring(
//               positions[i].start,
//               positions[i].end,
//             ),
//             style: const TextStyle(color: Colors.blue),
//             recognizer: new TapGestureRecognizer()
//               ..onTap = () {launchUrl(Uri.parse(text.substring(
//                 positions[i].start,
//                 positions[i].end,
//               ),)) ;
//               },
//           ),
//         );
//       }
//       spans.add(TextSpan(text: text.substring(positions.last.end)));
//     }
//     return spans;
//   }
// }
//
// class UrlPosition{
//   int start;
//   int end;
//   UrlPosition({required this.start,required this.end});
// }
