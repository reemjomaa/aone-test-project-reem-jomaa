


abstract class API {
  //##########  Base Url  ##########
  static const String baseUrl = 'https://conference.aone.sa/';


  //########### AUTH ###############
  static const String requestOtp = 'api/auth/request-otp';
  static const String verify = 'api/auth/verify-otp';
  static const String logout = 'api/auth/logout';


  static const String getAllSpeakers = 'api/attendances/speakers';
  static const String getAllFeaturesSpeakers = 'api/attendances/speakers/featured';
  static const String getThemes = 'api/topics';
  static const String getEventDetails = 'api/event';

}

