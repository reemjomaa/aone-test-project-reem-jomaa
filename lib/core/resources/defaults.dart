
import 'package:flutter/services.dart';
import 'enums.dart';
import 'localization.dart';

abstract class Default {
  static const AppLocalization defaultLocale = AppLocalization.En;

  static const Role defaultRole = Role.guest;

  static const String appTitle = 'Aone';


  static preferredOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

}
