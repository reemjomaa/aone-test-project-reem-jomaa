import 'package:flutter/material.dart';

import 'defaults.dart';

enum AppLocalization { Ar, En }

extension AppLocalizations on AppLocalization {
  String get value {
    switch (this) {
      case AppLocalization.Ar:
        return 'ar';
      case AppLocalization.En:
        return 'en';
      default:
        return Default.defaultLocale.value;
    }
  }

  Locale get locale {
    switch (this) {
      case AppLocalization.Ar:
        return Locale('ar');
      case AppLocalization.En:
        return Locale('en');
      default:
        return Locale(Default.defaultLocale.value);
    }
  }
}
