abstract class GlobalImages {
  static const String appLogo = 'assets/global/logo.png';
  static const String emailImage = 'assets/images/email_image.svg';
  static const String forgetPasswordImage = 'assets/images/forgit_password_image.svg';
  static const String loginImage = 'assets/images/loginImage.png';
  static const String registerImage = 'assets/images/registerImage.png';
  static const String subsecritionImage = 'assets/images/subsecriton_image.png';
  static const String supportImage = 'assets/images/support_image.svg';
  static const String successImage = 'assets/images/success_image.png';
  static const String accountSuccessImage = 'assets/images/account_success_image.png';
  static const String discountImage = 'assets/images/discount_product_image.png';
  static const String newImage = 'assets/images/new_image.png';
  static const String drawerIcon = 'assets/images/drawerIcon.svg';
  static const String primaryColorBookIcon = 'assets/images/primary_color_book_icon.svg';
  static const String downloadIcon = 'assets/images/downloadIcon.svg';
  static const String refrenceIcon = 'assets/images/refrenceIcon.png';
  static const String shippingPaymentIcon = 'assets/images/shipping_icon.svg';
  static const String whiteBookIcon = 'assets/images/white_book_icon.svg';
  static const String shareIcon = 'assets/images/share_icon.svg';
  static const String addressIcon = 'assets/images/address_icon.svg';
  static const String guestImage = 'assets/images/guest_user.png';
  static const String anonymousUserImage = 'assets/images/anonymous_user.jpg';
  static const String appLogoUrl = 'https://kitabialhadif.com/website/assets/img/logo.svg';
  static const String guestUserUrl = 'https://gwthani.vegasds.com/mobile/customers/public/images/default_3.jpg';

  ///go there and check
  static const String exhibitionDetailImage = 'assets/images/exhibition_detail_image.jpg';
  static const String whatsappIcon = 'assets/icons/whatsapp_icon.svg';
  static const String whiteReadingIcon = 'assets/images/white_reading_icon.svg';
  static const String whiteLiseanerIcon = 'assets/images/white_liseaner_icon.svg';
  static const String downloadRoundedIcon = 'assets/images/downloadRoundedIcon.png';
  static const String dropDragIcon = 'assets/images/dropDragIcon.svg';
  static const String paymentDoneIcon = 'assets/images/paymentDoneIcon.svg';
  static const String dateIcon = 'assets/images/dateIcon.svg';
  static const String editAddressIcon = 'assets/images/editAddressIcon.svg';
  static const String personSpeckingIcon = 'assets/images/personSpeckingIcon.svg';
  static const String greyLiseanerIcon = 'assets/images/grey_liseaner_icon.svg';
  static const String categoryOverLayIcon = 'assets/images/category_overlay.png';
  static const String eyeIcon = 'assets/icons/eye.svg';



  //background
  static const String backgroundTopImage = 'assets/images/backgroundTop.png';
  static const String backgroundUnderImage = 'assets/images/backgroundUnder.png';
  static const String drawerBackgroundImage = 'assets/images/drawerBackground.png';
  static const String profileBackgroundImage = 'assets/images/profileBackground.png';






  //layer images
  static const String layer1Image = 'assets/images/layer1.png';
  static const String layer2Image = 'assets/images/layer2.png';
  static const String layer3Image = 'assets/images/layer3.png';
  static const String layer4Image = 'assets/images/layer4.png';


  //social media image
  static const String googleIcon = 'assets/icons/socialMedia/googleIcon.svg';
  static const String facebookIcon = 'assets/icons/socialMedia/facebookIcon.svg';
  static const String appleIdIcon = 'assets/icons/socialMedia/appleIcon.svg';

  //social media image
  static const String shiningStarIcon = 'assets/icons/product_detail_icons/shining_star_icon.svg';
  static const String unShiningStarIcon = 'assets/icons/product_detail_icons/unShining_star_icon.svg';
  static const String lightUnShiningStarIcon = 'assets/icons/product_detail_icons/light_unShining_star_icon.svg';
  static const String videoIcon = 'assets/icons/product_detail_icons/vidio_icon.svg';
  static const String fullScreenIcon = 'assets/icons/product_detail_icons/full_screen_icon.svg';
  static const String paperBookIcon = 'assets/icons/product_detail_icons/paper_book_icon.svg';
  static const String liseanerBookIcon = 'assets/icons/product_detail_icons/liseaner_book_icon.svg';
  static const String elctronicBookIcon = 'assets/icons/product_detail_icons/elctronic_book_icon.svg';
  static const String giftIcon = 'assets/icons/product_detail_icons/gift_icon.svg';
  static const String addToCartIcon = 'assets/icons/product_detail_icons/add_to_cart_icon.svg';

  //point box icons
  static const String redDiamondIcon = 'assets/icons/point_box_icons/red_diamond.svg';
  static const String greenDiamondIcon = 'assets/icons/point_box_icons/green_diamond.svg';
  static const String blueDiamondIcon = 'assets/icons/point_box_icons/blue_diamond.svg';
  static const String blackDiamondIcon = 'assets/icons/point_box_icons/black_diamond.svg';
  static const String lowPointIcon = 'assets/icons/point_box_icons/low_point.svg';
  static const String heightPointIcon = 'assets/icons/point_box_icons/hieght_point.svg';

  //social media image
  static const String orderReceiveSelectedIcon = 'assets/icons/stepIcon/order_receve_selected.png';
  static const String companySelectedIcon = 'assets/icons/stepIcon/company_selected.png';
  static const String deliverySelectedIcon = 'assets/icons/stepIcon/delivery_selected.png';
  static const String companyUnSelectedIcon = 'assets/icons/stepIcon/company_unselected.png';
  static const String inWayUnSelectedIcon = 'assets/icons/stepIcon/in_way_unselected.png';


  //drawer images
  static const String settingIcon = 'assets/icons/drawerIcons/settingIcon.svg';
  static const String diamondIcon = 'assets/icons/drawerIcons/daimondIcon.svg';
  static const String favoriteOutLineIcon = 'assets/icons/drawerIcons/favoriteIcon.svg';
  static const String notificationOutLineIcon = 'assets/icons/drawerIcons/notificationIcon.svg';
  static const String privacyPolicyIcon = 'assets/icons/drawerIcons/privacyPolicyIcon.svg';
  static const String shippingIcon = 'assets/icons/drawerIcons/shippingIcon.svg';
  static const String callMeIcon = 'assets/icons/drawerIcons/callMeIcon.svg';

  static const String profileIcon = 'assets/icons/drawerIcons/profileIcon.svg';
  static const String subecretionIcon = 'assets/icons/drawerIcons/subecretionIcon.svg';
  static const String bookMarkIcon = 'assets/icons/drawerIcons/bookMarkIcon.svg';
  static const String myActivitiesIcon = 'assets/icons/drawerIcons/myActivitiesIcon.svg';
  static const String adsIcon = 'assets/icons/drawerIcons/adsIcon.svg';
  static const String globalDrawerIcon = 'assets/icons/drawerIcons/globalDrawerIcon.svg';
  static const String mediaIcon = 'assets/icons/drawerIcons/mediaIcon.svg';
  static const String helpCenterIcon = 'assets/icons/drawerIcons/helpCenterIcon.svg';
  static const String aboutAppIcon = 'assets/icons/drawerIcons/aboutAppIcon.svg';
  static const String logoutIcon = 'assets/icons/drawerIcons/logoutIcon.svg';
  static const String drawerFaceBookIcon = 'assets/icons/drawerIcons/facebookIcon.svg';
  static const String drawerTwitterIcon = 'assets/icons/drawerIcons/twitterIcon.svg';
  static const String drawerInstagramIcon = 'assets/icons/drawerIcons/instagramIcon.svg';
  static const String drawerYouTubeIcon = 'assets/icons/drawerIcons/youtube.svg';
  static const String deleteAccountIcon = 'assets/icons/drawerIcons/delete_account_icon.svg';

  //profile image
  static const String myInformationIcon = 'assets/icons/profileIcons/informationIcon.svg';
  static const String profileCartIcon = 'assets/icons/profileIcons/cartIcon.svg';
  static const String profileCategoryIcon = 'assets/icons/profileIcons/categoriesIcon.svg';
  static const String profileLockIcon = 'assets/icons/profileIcons/lockIcon.svg';
  static const String profileMarkerIcon = 'assets/icons/profileIcons/markerIcon.svg';
  static const String profileSupportIcon = 'assets/icons/profileIcons/supportCardIcon.svg';


  //bottom nav bar image
  static const String cartIcon = 'assets/icons/bottomIcon/cartIcon.svg';
  static const String homeIcon = 'assets/icons/bottomIcon/homeIcon.svg';
  static const String libraryIcon = 'assets/icons/bottomIcon/libraryIcon.svg';
  static const String profileNavBarIcon = 'assets/icons/bottomIcon/profileIcon.svg';
  static const String qrIcon = 'assets/icons/bottomIcon/qrIcon.svg';


  //payment stepper icons
  static const String unSelectedAddressIcon = 'assets/icons/payment_stepper_images/unSelectedAddressIcon.svg';
  static const String unSelectedCartIcon = 'assets/icons/payment_stepper_images/unSelectedCartIcon.svg';
  static const String unSelectedCardIcon = 'assets/icons/payment_stepper_images/unSelectedCardIcon.svg';
  static const String unSelectedDoneIcon = 'assets/icons/payment_stepper_images/unSelectedDoneIcon.svg';

  //payment type icons
  static const String bankTransformationIcon = 'assets/icons/payment_type_icons/bankTransformationIcon.png';
  static const String cachIcon = 'assets/icons/payment_type_icons/cachIcon.png';
  static const String cridgetCardIcon = 'assets/icons/payment_type_icons/cridgetCardIcon.png';
  static const String paypalIcon = 'assets/icons/payment_type_icons/paypalIcon.png';

  //payment type icons
  static const String openedIcon = 'assets/icons/support_card_icons/oppened_icon.svg';
  static const String closedIcon = 'assets/icons/support_card_icons/cloased_icon.svg';
  static const String answeredIcon = 'assets/icons/support_card_icons/answered_icon.svg';

}