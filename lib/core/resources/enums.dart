import 'package:flutter/material.dart';

import '../style/inputs.dart';
import 'defaults.dart';
import 'localization.dart';

enum HttpMethod { GET, POST, PUT, DELETE }

enum VarType { INTEGER, STRING, DOUBLE, BOOL }

enum Role { user, guest }

enum ToastStatus { success, fail, warning }

abstract class AppEnum {
  static AppLocalization getLocaleFromName(String? s) {
    if (s != null)
      for (AppLocalization locale in AppLocalization.values) {
        if (locale.value == s) return locale;
      }
    return Default.defaultLocale;
  }

  static Role getRoleFromName(String? s) {
    if (s != null)
      for (Role role in Role.values) {
        if (role.name == s) {
          return role;
        }
      }
    return Default.defaultRole;
  }
}

enum FieldTypeEnum {
  MainTheme,
  BorderLess,
  LabelTheme;

  InputDecorationTheme getTextFieldDecoration() {
    switch (this) {
      case MainTheme:
        return AppInputFieldTheme.mainThemeDecoration();
      case BorderLess:
        return AppInputFieldTheme.borderLessDecoration();
      case LabelTheme:
        return AppInputFieldTheme.labelThemeDecoration();
    }
  }
}

enum VariableStatus {
  Loading,
  HasData,
  Error,
}
