import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InitialError extends StatelessWidget {
  final Future Function()? refresh;
  final String error;
  const InitialError({super.key, this.refresh, required this.error});

  @override
  Widget build(BuildContext context) {
    if (refresh != null) {
      return RefreshIndicator(
        onRefresh: refresh!,
        child: Stack(
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // Assets.logoWithoutName.image(width: 100,height: 100,fit: BoxFit.fill),
                  const SizedBox(height: 10,),
                  Text(
                    error,
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.bold),
                  ),
                ],
              ),
            ),
            ScrollConfiguration(
              behavior: MaterialScrollBehavior().copyWith(overscroll: false),
              child: SingleChildScrollView(
                child: SizedBox(height: Get.height + 1, width: Get.width),
              ),
            )
          ],
        ),
      );
    }
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 16),
            child: Text(
              error,
              maxLines: 3,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
