import 'package:aone/core/style/font_weight.dart';
import 'package:aone/core/widgets/app_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PageError extends StatelessWidget {
  final Function() retry;
  final String error;
  const PageError({super.key, required this.retry, this.error = 'Some Error'});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Wrap(
          alignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 12,
          runSpacing: 6,
          children: [
            Text(
              error,
              style: Theme.of(context).textTheme.titleSmall!.copyWith(fontWeight: AppFontWeight.bold),
            ),
            AppButton(
              text: "Try Again".tr,
              onTap: ()async=> retry(),
              margin: EdgeInsets.symmetric(horizontal: Get.width*0.3),
              withLoading: false,
            ),
            // ElevatedButton(
            //   onPressed: retry,
            //   child: Text('retry'),
            // ),
          ],
        ),
      ),
    );
  }
}
