import 'package:aone/core/models/response_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller.dart';
import '../widgets/initial_error.dart';
import '../widgets/initial_loading.dart';
import '../widgets/loading.dart';
import '../widgets/page_error.dart';

// ignore: must_be_immutable
class GridViewPagination<T> extends StatelessWidget {
  final String tag;
  final Future<ResponseModel> Function(int, CancelToken cancel) fetchApi;
  final T Function(Map<String, dynamic> json) fromJson;
  final double closeToListEnd;
  final ScrollController? scrollController;
  final SliverGridDelegate gridDelegate;
  final Widget Function(BuildContext, int, T item) itemBuilder;
  final bool shrinkWrap;
  final Axis scrollDirection;
  final EdgeInsetsGeometry? padding;
  final ScrollPhysics? physics;
  bool refresh;
  final bool reverse;
  final Widget initialLoading;
  final Widget loading;
  final Widget Function(String)? errorWidget;

  GridViewPagination({
    super.key,
    required this.tag,
    required this.fetchApi,
    required this.fromJson,
    required this.itemBuilder,
    required this.gridDelegate,
    this.scrollController,
    this.shrinkWrap = false,
    this.padding,
    this.scrollDirection = Axis.vertical,
    this.physics,
    this.refresh = true,
    this.reverse = false,
    this.loading = const PageLoading(),
    this.initialLoading = const InitialLoading(),
    this.errorWidget,
    this.closeToListEnd = 500,
  }) {
    if (physics is NeverScrollableScrollPhysics) {
      assert(scrollController != null);
      refresh = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final PaginationController<T> controller = Get.put(
      PaginationController(
        fromJson: fromJson,
        fetchApi: fetchApi,
        closeToListEnd: closeToListEnd,
        scrollController: scrollController,
      ),
      tag: tag,
    );
    return Obx(
      () {
        if (controller.data.loading) {
          return initialLoading;
        } else if (controller.data.hasError &&
            controller.data.valueLength == 0) {
          return errorWidget?.call(controller.data.error!) ??
              InitialError(
                error: controller.data.error!,
                refresh:
                    refresh ? () async => await controller.refreshData() : null,
              );
        } else {
          if (refresh) {
            return RefreshIndicator(
              onRefresh: () async => await controller.refreshData(),
              child: Obx(() {
                return GridView.builder(
                  controller: physics is NeverScrollableScrollPhysics
                      ? null
                      : controller.scrollController,
                  gridDelegate: gridDelegate,
                  shrinkWrap: shrinkWrap,
                  physics: physics ?? AlwaysScrollableScrollPhysics(),
                  padding: padding,
                  reverse: reverse,
                  scrollDirection: scrollDirection,
                  itemCount: controller.data.valueLength +
                      (!controller.isFinished ? 1 : 0),
                  itemBuilder: (context, index) {
                    if (index == controller.data.valueLength) {
                      return Obx(
                        () {
                          if (controller.loading) {
                            return loading;
                          } else if (controller.data.hasError) {
                            return PageError(
                                retry: () => controller.loadData());
                          } else {
                            return SizedBox();
                          }
                        },
                      );
                    }
                    return itemBuilder(
                      context,
                      index,
                      controller.data.value![index],
                    );
                  },
                );
              }),
            );
          } else {
            return Obx(
              () => GridView.builder(
                controller: physics is NeverScrollableScrollPhysics
                    ? null
                    : controller.scrollController,
                gridDelegate: gridDelegate,
                shrinkWrap: shrinkWrap,
                physics: physics,
                padding: padding,
                reverse: reverse,
                scrollDirection: scrollDirection,
                itemCount: controller.data.valueLength +
                    (!controller.isFinished ? 1 : 0),
                itemBuilder: (context, index) {
                  if (index == controller.data.valueLength) {
                    return Obx(
                      () {
                        if (controller.loading) {
                          return loading;
                        } else {
                          return PageError(retry: () => controller.loadData());
                        }
                      },
                    );
                  }
                  return itemBuilder(
                    context,
                    index,
                    controller.data.value![index],
                  );
                },
              ),
            );
          }
        }
      },
    );
  }
}
