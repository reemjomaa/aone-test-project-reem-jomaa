
import 'package:flutter/material.dart';

class SkeletonWidget extends StatelessWidget {
  final double? width;
  final double? height;
  final double? radius;
  final EdgeInsetsDirectional? padding;
  final EdgeInsetsDirectional? margin;
  final bool isCircular;

  SkeletonWidget({this.padding,this.margin,this.height,this.width,this.radius,this.isCircular=false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      padding: padding,
      margin: margin,
      decoration: BoxDecoration(
        image: const DecorationImage(
          image: AssetImage(
              'assets/gif/loading.gif'),
          fit: BoxFit.fill,
        ),
        shape: isCircular?BoxShape.circle:BoxShape.rectangle,
        borderRadius:isCircular? null: BorderRadius.all(Radius.circular(radius??12)),
      ),
    );
  }
}
