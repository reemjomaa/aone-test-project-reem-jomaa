import 'package:aone/core/resources/enums.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../resources/localization.dart';


class AppToast extends StatelessWidget {
  final String title;
  final String message;
  final AppLocalization locale;
  final ToastStatus status;

  const AppToast({
    Key? key,
    required this.title,
    required this.message,
    required this.locale,
    this.status = ToastStatus.success,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width,
      padding: locale == AppLocalization.Ar
          ? const EdgeInsets.only(right: 5)
          : const EdgeInsets.only(left: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.secondaryColor,
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: status == ToastStatus.success
              ? AppColors.whiteColor.withOpacity(0.3)
              : Colors.redAccent,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                color: AppColors.whiteColor,
                fontWeight: AppFontWeight.bold,
              ),
            ),
            Text(
              message,
              style: TextStyle(
                color: AppColors.whiteColor,
                fontWeight: AppFontWeight.regular,
              ),
            )
          ],
        ),
      ),
    );
  }
}
