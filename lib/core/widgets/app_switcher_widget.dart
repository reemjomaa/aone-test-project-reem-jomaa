import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_switch/flutter_switch.dart';

class AppSwitcherWidget extends StatelessWidget {
  final Function(bool) afterSwitch;
  final bool isSelected;
  AppSwitcherWidget({required this.afterSwitch,required this.isSelected});

  late ValueNotifier<bool> isSwitched = ValueNotifier<bool>(isSelected);


  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: isSwitched,
        builder: (context,bool switched,_) {
          return FlutterSwitch(
            padding: 0,
            height: 20.h,
            width: 40.w,
            toggleSize: 20.r,
            value: switched,
            borderRadius: 30.0,
            activeColor: AppColors.secondaryColor,
            inactiveColor: AppColors.authBackgroundColor,
            toggleColor: AppColors.whiteColor,
            onToggle: (val) {
              isSwitched.value = val;
              afterSwitch(val);
            },
          );
        }
    );
  }

}