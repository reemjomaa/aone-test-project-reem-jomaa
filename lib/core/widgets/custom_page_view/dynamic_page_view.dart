import 'package:flutter/material.dart';

import 'size_reporter_widget.dart';

class DynamicPageView extends StatefulWidget {
  final PageController pageController;
  final List<Widget> item;
  const DynamicPageView({
    Key? key,
    required this.item,
    required this.pageController
  }) : super(key: key);

  @override
  _DynamicPageViewState createState() => _DynamicPageViewState();
}

class _DynamicPageViewState extends State<DynamicPageView> with SingleTickerProviderStateMixin {

  int _currentPage = 0;
  late final List<double> _height = List.generate(
      widget.item.length, (index) => 0.0
  ).toList();
  late final PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = widget.pageController..addListener(() {
      final newPage = _pageController.page!.round();
      if (_currentPage != newPage) {
        setState(() => _currentPage = newPage);
      }
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  double get _currentHeight => _height[_currentPage];

  @override
  Widget build(BuildContext context) {
    return TweenAnimationBuilder<double>(
      tween: Tween<double>(begin: 0.0, end: _currentHeight),
      duration: const Duration(milliseconds: 300),
      builder: (context, value, child) =>
          SizedBox(height: value, child: child),
      child: PageView.builder(
        controller: _pageController,
        itemCount: widget.item.length,

        itemBuilder: (context, index) {
          return OverflowBox(
              alignment: Alignment.topLeft,
              minHeight: 0,
              maxHeight: double.infinity,
              child: SizeReporterWidget(
                  child: widget.item[index],
                  onSizeChange: (size) => setState(() =>
                  _height[index] = size.height)
              )
          );
        },
      ),
    );
  }
}

