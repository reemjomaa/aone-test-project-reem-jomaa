
import 'package:aone/core/resources/enums.dart';
import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';


class AppTextField extends StatelessWidget {
  final TextEditingController controller;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final String? initialValue;
  final String? hint;
  final String? label;
  final TextInputType? keyboardType;
  final bool isSecure;
  final Function()? onTap;
  final EdgeInsetsGeometry? contentPadding;
  final bool isEnabled;
  final String? Function(String?)? validator;
  final String? Function(String?)? onFieldSubmitted;
  final String? Function(String?)? onChanged;
  final TextStyle? textStyle;
  final Color? color;
  final TextAlign textAlign;
  final FieldTypeEnum? type;
  final int minLine;
  final int maxLine;
  final String? errorText;
  const AppTextField(
      this.controller, {
      this.hint,
      this.label,
      this.suffixIcon,
      this.onTap,
      this.textStyle,
      this.errorText,
      this.isEnabled = true,
      this.contentPadding,
      this.onFieldSubmitted,
      this.onChanged,
      this.isSecure = false,
      this.keyboardType,
      this.textAlign = TextAlign.start,
      this.color,
      this.prefixIcon,
      this.initialValue,
      this.validator,
      this.minLine=1,
      this.maxLine=1,
      this.type= FieldTypeEnum.MainTheme,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          inputDecorationTheme: type!.getTextFieldDecoration(),
          textSelectionTheme: TextSelectionThemeData(
              selectionColor: AppColors.secondaryColor.withOpacity(0.5),
              selectionHandleColor: AppColors.secondaryColor,
              cursorColor: AppColors.secondaryColor,
          )
      ),
      child: TextFormField(
        controller: controller,
        validator: validator,
        keyboardType: keyboardType,
        obscureText: isSecure,
        readOnly: !isEnabled,
        onTap: onTap,
        minLines: minLine,
        maxLines: maxLine,
        initialValue: initialValue,
        style: textStyle,
        textAlign: textAlign,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChanged,
        decoration: InputDecoration(
            contentPadding: contentPadding,
            isDense: true,
            prefixIcon: prefixIcon,
            suffixIcon: suffixIcon,
            hintText: hint,
            label: label!=null?Text(label??''):null,
            errorText: errorText,
        ),
      ),
    );
  }
}
