import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'loading.dart';

class AppButton extends StatelessWidget {
  final String text;
  final Future Function() onTap;
  final bool withLoading;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final TextStyle? style;
  final double? width;
  final double radius;
  final double? height;
  final Color borderColor;
  final Color? fillColor;
  final Color? textColor;
  final Widget? moreWidget;

  const AppButton(
      {super.key,
      required this.text,
      required this.onTap,
      this.style,
      this.width,
      this.height,
      this.withLoading = true,
      this.radius=10,
      this.fillColor,
      this.textColor,
      this.borderColor= AppColors.primaryColor,
      this.moreWidget,
      this.padding= const EdgeInsets.symmetric(vertical: 8),
      this.margin = const EdgeInsets.symmetric(horizontal: 16)});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        if (withLoading) {
          Loading.overlayLoading(context);
          await onTap();
        } else {
          onTap();
        }
      },
      child: Container(
        width: width,
        height: height,
        margin: margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius.r),
          color: fillColor??AppColors.secondaryColor,
          border: Border.all(color: borderColor)
        ),
        alignment: Alignment.center,
        padding: padding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: moreWidget!=null,
              child: moreWidget??const SizedBox(),
            ),
            Text(
              text,
              style: style??Theme.of(context).textTheme.titleSmall!.copyWith(color: AppColors.primaryColor)
            ),
          ],
        ),
      ),
    );
  }
}
