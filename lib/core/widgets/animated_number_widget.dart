import 'package:animated_flip_counter/animated_flip_counter.dart';
import 'package:aone/core/style/colors.dart';
import 'package:aone/core/style/font_weight.dart';
import 'package:flutter/material.dart';

class AnimatedNumberWidget extends StatefulWidget {
  const AnimatedNumberWidget({
    Key? key,
    required this.number,
  }) : super(key: key);

  final int number;

  @override
  State<AnimatedNumberWidget> createState() => _AnimatedNumberWidgetState();
}

class _AnimatedNumberWidgetState extends State<AnimatedNumberWidget> {
  int number = 000;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.endOfFrame.then(
      (_) {
        if (mounted) {
          setState(() {
            number = widget.number;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.ltr,
      child: AnimatedFlipCounter(
        curve: Curves.easeInOut,
        duration: const Duration(seconds: 2),
        value: number,
        textStyle: Theme.of(context).textTheme.headlineSmall!.copyWith(
            fontWeight: AppFontWeight.semiBold,
            color: AppColors.textPrimaryColor),
      ),
    );
  }
}
