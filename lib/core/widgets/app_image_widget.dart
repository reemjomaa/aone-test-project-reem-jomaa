import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'image_error_widget.dart';
import 'image_loading_widget.dart';

enum ImageType { network, file, cachedNetwork, asset }

class AppImageWidget extends StatelessWidget {
  const AppImageWidget(
    this.path, {
    Key? key,
    this.width,
    this.height,
    this.padding = EdgeInsets.zero,
    this.fit = BoxFit.cover,
    this.borderRadius,
    this.boxShadow,
    this.border,
    this.color,
    this.backgroundColor,
    this.errorPadding = EdgeInsets.zero,
    required this.type,
    this.placeholderWidth,
    this.placeholderHeight,
    this.errorWidth = 30,
    this.boxShape,
  }) : super(key: key);

  final String path;
  final double? width;
  final double? height;
  final EdgeInsetsGeometry padding;
  final BoxFit fit;
  final Color? color;
  final BorderRadius? borderRadius;
  final List<BoxShadow>? boxShadow;
  final BoxShape? boxShape;
  final Border? border;
  final Color? backgroundColor;
  final EdgeInsetsGeometry errorPadding;
  final ImageType type;
  final double? placeholderWidth;
  final double? placeholderHeight;
  final double errorWidth;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      padding: padding,
      decoration: BoxDecoration(
        color: backgroundColor,
        boxShadow: boxShadow,
        // shape: boxShape??BoxShape.rectangle,
        border: border,
        // borderRadius: borderRadius,
      ),
      child: ClipRRect(
        borderRadius: borderRadius??BorderRadius.zero,
        child: Builder(
          builder: (context) {
            switch (type) {
              case ImageType.cachedNetwork:
                return CachedNetworkImage(
                  imageUrl: path,
                  fit: fit,
                  color: color,
                  placeholder: (context, url) => ImageLoadingWidget(
                    width: placeholderWidth,
                    height: placeholderHeight,
                  ),
                  errorWidget: (context, url, error) => Padding(
                    padding: errorPadding,
                    child: ImageErrorWidget(width: errorWidth),
                  ),
                );
              case ImageType.network:
                return Image.network(
                  path,
                  errorBuilder: (context, _, i) => Padding(
                    padding: errorPadding,
                    child: ImageErrorWidget(width: errorWidth),
                  ),
                  loadingBuilder: (
                    BuildContext context,
                    Widget child,
                    ImageChunkEvent? loadingProgress,
                  ) {
                    if (loadingProgress == null) return child;
                    return ImageLoadingWidget(
                      width: placeholderWidth,
                      height: placeholderHeight,
                    );
                  },
                  fit: fit,
                  color: color,
                );
              case ImageType.asset:
                return Image.asset(
                  path,
                  errorBuilder: (context, _, i) =>
                      ImageErrorWidget(width: errorWidth),
                  fit: fit,
                  color: color,
                );
              case ImageType.file:
                return Image.file(
                  File(path),
                  fit: fit,
                  errorBuilder: (context, _, i) =>
                      ImageErrorWidget(width: errorWidth),
                );
              default:
                return const SizedBox();
            }
          },
        ),
      ),
    );
  }
}
