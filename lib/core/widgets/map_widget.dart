import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
 import 'package:google_maps_flutter/google_maps_flutter.dart';



class MapWidget extends StatelessWidget {
  bool? showInfoWindows;
  LatLng? startLocation;
  double height;
  Marker marker;
  double zooming;

  MapWidget({required this.zooming,required this.height,required this.marker,this.showInfoWindows=true,this.startLocation});


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height:height,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.r),
        child: GoogleMap(
          myLocationButtonEnabled: true,
          zoomControlsEnabled: true,
          mapType: MapType.normal,
          zoomGesturesEnabled: true,
          scrollGesturesEnabled: true,
          initialCameraPosition: CameraPosition(
            target: startLocation!,
            zoom: zooming,
          ),
          markers: Set.from([marker]),
        ),
      ),
    );
  }

}
