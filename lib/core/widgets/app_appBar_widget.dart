
import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppAppbarWidget extends StatelessWidget {
  const AppAppbarWidget({
    Key? key,
    required this.centerWidget,
    required this.leadingIcon,
    required this.trailingIcon,
    required this.leadingOnTap,
    required this.trailingOnTap,
    this.withTopPadding= false
  }) : super(key: key);
  final Widget centerWidget;
  final Widget leadingIcon;
  final Widget trailingIcon;
  final VoidCallback? leadingOnTap;
  final VoidCallback trailingOnTap;
  final bool withTopPadding;


  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.primaryColor,
      padding: EdgeInsets.only(top: Get.mediaQuery.viewPadding.top+10,),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: leadingOnTap,
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: leadingIcon,
              ),
            ),
            const SizedBox(width: 6),
            centerWidget,
            const SizedBox(width: 6),
            InkWell(
              onTap: trailingOnTap,
              child: Padding(
                padding: const EdgeInsets.all(4),
                child: trailingIcon,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
