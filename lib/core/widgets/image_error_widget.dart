import 'package:flutter/material.dart';

class ImageErrorWidget extends StatelessWidget {
  const ImageErrorWidget({
    Key? key,
    this.width = 30,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Icon(
        Icons.error_outline_rounded,
        size: width,
      ),
    );
  }
}
