import 'package:aone/core/gen/assets.gen.dart';
import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

class Loading {
  static overlayLoading(BuildContext context) => showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Assets.lottie.airplanLoading.lottie(
              ),
              TextAnimator(
                 "Loading...",
                 atRestEffect: WidgetRestingEffects.pulse(),
                 style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                   fontWeight: FontWeight.bold,
                   color: AppColors.secondaryColor
                 ),
              ),
            ],
          ),
        ),
      );
    },
  );
}




