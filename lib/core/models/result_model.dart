

class ResultModel<E, D>{
  final E? error;
  final D? data;

  ResultModel({this.data, this.error}){
    assert(data != null || error != null);
  }

  bool get hasDataAndError => data != null && error != null;

  bool get hasDataOnly => data != null && error == null;

  bool get hasErrorOnly => data == null && error != null;

}