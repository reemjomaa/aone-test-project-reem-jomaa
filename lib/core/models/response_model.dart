

enum ErrorCode {
  CANCELED,
  NO_INTERNET,
  UN_AUTHORIZED,
  VALIDATION_ERROR,
  SERVER_ERROR,
  NOT_ENOUGH_MONEY,
}

class ResponseModel {
  var data;
  late int? status;
  late bool success;
  dynamic message;
  dynamic extra;
  List? errors;
  ErrorCode? code;

  ResponseModel({this.status, this.data,this.errors, this.message, this.code,required this.success,this.extra});


  ResponseModel.fromJson(Map<String, dynamic> json,{String? extraParam}) {
    status = json['status'] ?? 200;
    success = json['success'] ?? false;
    message = json['message'] ?? 'IDK';
    data = json['data'];
    extra = json[extraParam];
    errors = json['errors'];
  }

  ErrorCode? getCode(String? code) {
    if (code != null) {
      return ErrorCode.values
          .firstWhere((element) => element.name == code, orElse: null);
    }
    return null;
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['data'] = this.data;
    data['message'] = this.message;
    return data;
  }
}
