import 'dart:developer';

import 'package:aone/core/resources/enums.dart';
import 'package:get/get.dart';

class Obs {
  late Rx<VariableStatus> _status;
  String? _error;
  String? get error => _error;
  set error(value) {
    _error = value;
    if (value != null) {
      status = VariableStatus.Error;
    }
  }

  VariableStatus get status => this._status.value;
  set status(VariableStatus value) => this._status.value = value;

  bool get hasData => status == VariableStatus.HasData;
  bool get hasError => status == VariableStatus.Error;
  bool get loading => status == VariableStatus.Loading;

  refresh() => _status.refresh();
}

class ObsVar<T> extends Obs {
  T? _value;

  ObsVar(T? val) {
    if (val is List) {
      throw 'use ObsList instead ObsVar';
    }
    _value = val;
    if (_value == null)
      _status = (VariableStatus.Loading).obs;
    else
      _status = (VariableStatus.HasData).obs;
  }

  T? get value => _value;
  set value(val) {
    _value = val;
    if (_value == null)
      status = VariableStatus.Loading;
    else
      status = VariableStatus.HasData;
  }

  get reset {
    this._value = null;
    this.status = VariableStatus.Loading;
  }

  loger() {
    log('$_status');
    log('$_value');
    log(error!);
  }
}

class ObsList<T> extends Obs {
  List<T>? _value;
  List<T>? get value => _value;

  set value(List<T>? value) {
    if (value == null)
      status = VariableStatus.Loading;
    else if (value.isEmpty) {
      status = VariableStatus.Error;
      error = 'No Data';
    } else {
      status = VariableStatus.HasData;
      this._value = value;
      this.valueLength = value.length;
    }
  }

  RxInt? _valueLength;
  int get valueLength => this._valueLength!.value;
  set valueLength(value) => this._valueLength!.value = value;

  ObsList(List<T>? value) {
    this._value = value;
    if (_value == null || _value!.isEmpty) {
      _status = (VariableStatus.Loading).obs;
      _valueLength = 0.obs;
    } else {
      _status = (VariableStatus.HasData).obs;
      _valueLength = (value!.length).obs;
    }
  }

  T operator [](int index) => value![index];

  bool? get isEmpty => valueLength == 0;
  bool? get isNotEmpty => valueLength != 0;

  set valueAppend(List<T> value) {
    if (status != VariableStatus.HasData) {
      status = VariableStatus.HasData;
    }
    this._value = this._value! + value;
    if (this._value!.isEmpty && value.isNotEmpty)
      this._status = (VariableStatus.HasData).obs;
    this.valueLength += value.length;
  }

  ObsList<T> operator +(List<T> value) {
    if (status != VariableStatus.HasData && this.isNotEmpty!) {
      status = VariableStatus.HasData;
      refresh();
    }
    this._value = this._value! + value;
    if (this._value!.isEmpty && value.isNotEmpty)
      this._status = (VariableStatus.HasData).obs;
    this.valueLength += value.length;
    return this;
  }

  bool contains(bool Function(T e) condition) =>
      value!.indexWhere(condition) != -1;

  add(T value) {
    this._value!.add(value);
    this.valueLength++;
  }

  insert(int index, T element) {
    this.value!.insert(index, element);
    valueLength++;
  }

  removeAt(int index) {
    assert(index < this.valueLength);
    this._value!.removeAt(index);
    this.valueLength--;
  }

  Iterable<T> where(bool Function(T element) test) => value!.where(test);

  T firstWhere(bool Function(T element) test) => value!.firstWhere(test);

  int indexWhere(bool Function(T element) test) => value!.indexWhere(test);

  T? removeWhere(bool Function(T element) condition) {
    T? removed;
    value!.removeWhere((element) {
      if (condition(element)) {
        removed = element;
        return true;
      } else {
        return false;
      }
    });
    valueLength = value!.length;
    return removed;
  }

  Iterable<TT> map<TT>(TT Function(T) mapper) => _value!.map<TT>(mapper);

  void forEach(void Function(T element) iterable) => value!.forEach(iterable);

  get reset {
    this._value = [];
    this.valueLength = 0;
    this.status = VariableStatus.Loading;
  }

  @override
  refresh() {
    super.refresh();
    _valueLength?.refresh();
  }

  loger() {
    log('********   start   *********');
    this._value?.forEach((element) => log('$element'));
    log('********    end    *********');
    log('length: $valueLength');
    log('error: $error');
  }
}
