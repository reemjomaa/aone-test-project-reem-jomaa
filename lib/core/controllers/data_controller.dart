import 'dart:developer';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/resources/api.dart';
import 'package:get/get.dart' hide GetConnect, Response, FormData;
import 'dart:io';
import 'package:dio/dio.dart';
import '../routes.dart';
import 'app_controller.dart';

class DataController extends GetxService {
  AppController appController = Get.find();

  final bool withLog;
  DataController({this.withLog = true});


  //#################### API ########################
  Dio dio = Dio();

  Map<String, dynamic> get header => {
    'Authorization': 'Bearer ${appController.token}',
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };

  //==================== POST =======================
  Future<ResponseModel> postData({
    required String url,
    required body,
    Map<String, dynamic>? param,
    bool isFullURL = false,
    CancelToken? cancel,
    String? extraParam
  }) async {
    Response response;
    try {
      if (withLog) {
        log(isFullURL ? url : API.baseUrl + url);
        log('$body');
        if (body is FormData) {
          log('${body.fields}');
          log('${body.files}');
        }
        log('$param');
      }
      log(header.toString());
      response = await dio.post(
        isFullURL ? url : API.baseUrl + url,
        data: body,
        options: Options(headers: header, contentType: 'Application/json'),
        queryParameters: param,
        cancelToken: cancel,
      );
      if (withLog) {
        log('--------------- dataController ---------------');
        log(response.data.toString(), name: 'result');
      }
      if (response.data == null) throw 'connection error';
      ResponseModel responseModel = ResponseModel.fromJson(response.data,extraParam: extraParam);
      responseModel.success = true;
      return responseModel;
    } catch (error) {
      if (error is DioError) {
        if (withLog) {
          log(error.toString());
          log('${error.response}');
        }
        return handlingDioError(error);
      }
      return ResponseModel(success: false, message: 'No Internet');
    }
  }

  //================= GET ============================
  Future<ResponseModel> getData({
    required String url,
    Map<String, dynamic>? param,
    bool isFullURL = false,
    CancelToken? cancel,
    String? extraParam
  }) async {
    Response response;
    try {
      if (withLog) {
        log(isFullURL ? url : API.baseUrl + url);
        log('$param');
      }
      log('$header');
      response = await dio.get(
        isFullURL ? url : API.baseUrl + url,
        options: Options(headers: header),
        queryParameters: param,
        cancelToken: cancel,
      );
      if (withLog) {
        log('--------------- dataController ---------------');
        log('${response.data}');
      }
      if (response.data == null) throw '';
      ResponseModel responseModel = ResponseModel.fromJson(response.data,extraParam: extraParam);
      responseModel.success = true;
      return responseModel;
    } catch (error) {
      if (withLog) {
        log('--------------- dataController ---------------');
        log(error.toString());
      }
      if (error is DioError) {
        if (withLog) {
          log('here');
          log('${error.response}');
        }
        return handlingDioError(error);
      }
      return ResponseModel(success: false, message: 'No Internet');
    }
  }

  //======================= PUT =====================
  Future<ResponseModel> putData({
    required String url,
    body,
    Map<String, dynamic>? param,
    bool isFullURL = false,
    CancelToken? cancel,
  }) async {
    Response response;
    if (withLog) {
      log(isFullURL ? url : API.baseUrl + url);
      log('$body');
      if (body is FormData) {
        log('${body.fields}');
        log('${body.files}');
      }
      log('$param');
    }
    try {
      response = await dio.put(
        isFullURL ? url : API.baseUrl + url,
        data: body ?? "",
        options: Options(headers: header),
        queryParameters: param,
        cancelToken: cancel,
      );
      if (withLog) {
        log('--------------- dataController ---------------');
        log('################');
        log(response.data);
        log('################');
      }
      if (response.data == null) throw 'connection error';
      ResponseModel data = ResponseModel.fromJson(response.data);
      return data;
    } catch (error) {
      if (error is DioError) {
        return handlingDioError(error);
      }
      return ResponseModel(success: false, message: error.toString());
    }
  }

  //======================= DELETE =====================
  Future<ResponseModel> deleteData({
    required String url,
    body,
    Map<String, dynamic>? param,
    bool isFullURL = false,
    CancelToken? cancel,
  }) async {
    Response response;
    if (withLog) {
      log(isFullURL ? url : API.baseUrl + url);
      log('$body');
      if (body is FormData) {
        log('${body.fields}');
        log('${body.files}');
      }
      log('$param');
    }
    try {
      response = await dio.delete(
        isFullURL ? url : API.baseUrl + url,
        data: body ?? "",
        options: Options(headers: header),
        queryParameters: param,
        cancelToken: cancel,
      );
      if (withLog) {
        log('--------------- dataController ---------------');
        log('${response.data}');
      }
      if (response.data == null) throw 'connection error';
      ResponseModel data = ResponseModel.fromJson(response.data);
      return data;
    } catch (error) {
      if (error is DioError) {
        return handlingDioError(error);
      }
      if(error is SocketException){
        return ResponseModel(success: false, message: 'No Internet');
      }

      return ResponseModel(success: false, message: error.toString());
    }
  }



  ResponseModel handlingDioError(DioError error) {
    switch (error.type) {
      case DioErrorType.cancel:
        return ResponseModel(success: false, code: ErrorCode.CANCELED);
      case DioErrorType.connectTimeout:
        return ResponseModel(
          success: false,
          message: 'No Internet',
          code: ErrorCode.NO_INTERNET,
        );
      case DioErrorType.receiveTimeout:
        return ResponseModel(
          success: false,
          message: 'No Internet',
          code: ErrorCode.NO_INTERNET,
        );
      case DioErrorType.sendTimeout:
        return ResponseModel(
          success: false,
          message: 'No Internet',
          code: ErrorCode.NO_INTERNET,
        );
      case DioErrorType.response:
        switch (error.response!.statusCode) {
          case 403:

            Nav.offAll(Pages.login);
            //TODO: unauth
            return ResponseModel(
              success: false,
              message: 'No Auth'.tr,
              code: ErrorCode.UN_AUTHORIZED,
            );
          case 422:
          //TODO:
            return ResponseModel.fromJson(error.response!.data)
              ..code = ErrorCode.VALIDATION_ERROR;
          case 500:
            return ResponseModel(
              success: false,
              code: ErrorCode.SERVER_ERROR,
              message: 'Server Error',
            );
          default:
            if (error.response != null && error.response!.data != null) {
              try {
                return ResponseModel.fromJson(error.response!.data);
              } catch (_) {}
            }
            return ResponseModel(success: false, message: 'Unknown Error');
        }
      case DioErrorType.other:
        if (error.message.contains('SocketException') || error.message.contains('Connection reset by peer')) {
          return ResponseModel(
            success: false,
            message: 'No Internet',
            code: ErrorCode.NO_INTERNET,
          );
        }
        return ResponseModel(success: false, message: 'Unknown Error');
      default:
        return ResponseModel(success: false, message: 'Unknown Error');
    }
  }
}
