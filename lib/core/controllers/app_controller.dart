import 'dart:developer';
import 'package:aone/core/helpers/connectivity.dart';
import 'package:aone/core/models/user_model.dart';
import 'package:aone/core/resources/defaults.dart';
import 'package:aone/core/resources/enums.dart';
import 'package:aone/core/resources/localization.dart';
import 'package:aone/core/widgets/app_toast.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AppController extends GetxService {
  GetStorage box = GetStorage(Default.appTitle);

  FToast fToast = FToast();

  late Role role = Default.defaultRole;
  String token = '';

  final Rx<AppLocalization> _locale = Default.defaultLocale.obs;
  AppLocalization get locale => _locale.value;
  set locale(AppLocalization value) => _locale.value = value;

  updateLocale(String language) {
    var localization = getAppLanguageSymbol(language);
    if (localization == AppLocalization.Ar) {
      Get.updateLocale(AppLocalization.Ar.locale);
      setLocale(AppLocalization.Ar);
    } else if (localization == AppLocalization.En) {
      Get.updateLocale(AppLocalization.En.locale);
      setLocale(AppLocalization.En);
    }
  }

  String getAppLanguageLabel(){
    if (locale == AppLocalization.Ar) {
      return 'Arabic'.tr;
    } else if (locale == AppLocalization.En) {
      return 'English'.tr;
    }
    return '';
  }

  AppLocalization getAppLanguageSymbol(String language){
    if (language == 'Arabic') {
      return AppLocalization.Ar;
    } else if (language == 'English') {
      return AppLocalization.En;
    }
    return AppLocalization.En;
  }

  AppLocalization getAppLanguageFromLanguageSymbol(String language){
    if (language == 'ar') {
      return AppLocalization.Ar;
    } else if (language == 'en') {
      return AppLocalization.En;
    }
    return AppLocalization.Ar;
  }

  bool get isEnglish => locale == AppLocalization.En;
  bool get isArabic => locale == AppLocalization.Ar;

  final RxBool _connectivity = false.obs;
  bool get connectivity => this._connectivity.value;
  set connectivity(value) => this._connectivity.value = value;
  bool firstConnectivityTest = true;

  Future<bool> internetChecker() async {
    await Future.delayed(Duration(seconds: 2));
    bool checker = await MyConnectivity.check;
    if (checker&&!connectivity) {
      if(firstConnectivityTest){
        firstConnectivityTest= false;
      }else{
        showToast(Get.context!, message: 'Your internet connection has been restored'.tr,status: ToastStatus.success);
      }
      connectivity=!connectivity;
    }else if(!checker){
      if(firstConnectivityTest){
        firstConnectivityTest= false;
      }
      connectivity = false;
      showToast(Get.context!, message: 'No Internet, Please check your connection'.tr,status: ToastStatus.warning);
    }
    return connectivity;
  }

  @override
  void onInit() {
    internetChecker();
    super.onInit();
  }

  loadUserData() async {
    await box.initStorage;
    if (box.hasData('locale')) {
      locale = AppEnum.getLocaleFromName(box.read('locale'));
      if (locale != Default.defaultLocale) {
        Get.updateLocale(locale.locale);
        setLocale(locale);
      }
    }else{
      locale = getAppLanguageFromLanguageSymbol('en');
      Get.updateLocale(Locale('en', 'UK'));
    }
    if (box.hasData('token') && box.read('token') != '') {
      token = box.read('token');
    }
    role = AppEnum.getRoleFromName(box.read('role'));

  }


  removeUserData() async {
    await box.initStorage;
    if (box.hasData('token') && box.read('token') != '') {
      box.remove('token');
    }
    if (box.hasData('role')) {
      box.remove('role');
    }
    role=Role.guest;
  }


  setRole(Role role) {
    box.write('role', role.name);
    this.role = role;
  }

  setLocale(AppLocalization locale) {
    box.write('locale', locale.value);
    this.locale = locale;
  }

  setToken(String token) {
    box.write('token', token);
    this.token = token;
  }

  bool isGuestUser(){
    return role==Role.guest;
  }

  // ############ TOAST ###############
  showToast(BuildContext context,
      {String? title,
        required String message,
        ToastStatus status = ToastStatus.fail}) {
    fToast.init(Get.overlayContext!);
    try {
      fToast.removeCustomToast();
    } catch (_) {}
    FocusManager.instance.primaryFocus?.unfocus();
    if (title == null) {
      if (status == ToastStatus.success) {
        title = 'Success';
      } else if (status == ToastStatus.warning) {
        title = 'Warning';
      } else {
        title = 'Failure';
      }
    }
    fToast.showToast(
      child: AppToast(
        title: title,
        message: message,
        locale: locale,
        status: status,
      ),
      toastDuration: 2.seconds,
    );
  }




}
