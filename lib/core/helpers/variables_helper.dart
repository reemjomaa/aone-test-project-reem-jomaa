
import 'package:aone/core/resources/enums.dart';

abstract class VariablesHelper{
  static VarType getVariableType(dynamic value) {
    if (value is bool) {
      return VarType.BOOL;
    } else if (value is double) {
      return VarType.DOUBLE;
    } else if (value is int) {
      return VarType.INTEGER;
    } else if (value is String) {
      return VarType.STRING;
    }else{
      throw Exception('Exception');
    }
  }
}