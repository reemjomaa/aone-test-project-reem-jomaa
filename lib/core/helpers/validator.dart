

abstract class Validator {
  static String? phoneValidation(String phone) {
    final phoneRegex = RegExp(r'^09[0-9]{8}$');
    if (!phoneRegex.hasMatch(phone)) return 'Phone is not valid';
    if(phone.length<10) return 'Phone must be 10 numbers';
    return null;
  }

  static String? emailValidation(String? email) {
    if(email==null||email.isEmpty)
      return 'This Filed is required';
    email = email.trim();
    bool valid = RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
    return valid == false ? "Email is not valid" : null;
  }

  static String? moneyValidation(String? money) {
    money = money?.trim();
    bool valid = RegExp(r'^[0-9]+\.?[0-9]*$').hasMatch(money!);
    return valid == false ? "this value not money" : null;
  }

  static bool checkNumberIsMoney(String money) {
    money = money.trim();
    if (RegExp(r'^[0-9]+\.?[0-9]*$').hasMatch(money)) {
      return true;
    }
    return false;
  }

  static String? checkIfNumber(String number) {
    if(double.tryParse(number) != null)
      return null;
    else
    return 'This Filed must be numbers';
  }

  static String? notNullValidation(String? str) =>
      (str == null || str == '') ? 'This Filed is required' : null;

  static String? verifyCodeValidation(String str) =>
      (str == '' || str.length <5 ) ? 'Verification code must be 5 characters' : null;

  static String? cityValidation(String? input,int? cityId) {
    return (input != null && input.isNotEmpty && cityId == null) ? 'You must choose the city only from list' : null;
  }


  static String ratingValidation(double rate) =>
      (rate == 0) ? 'Rating must be upper than 0' : '';

  static String? passwordAndConfirmEqual(String password,String confirm) =>
      password != confirm ? 'Password and It\'s confirm are not equal' : null;


  static bool checkIfPasswordAndConfirmEqual(String password,String confirm) =>
      password != confirm ? false : true;

  static String? notNullValidationValue(String? str) =>
      (str == null || str == '') ? '' : null;

  static bool checkIfEmailValid(String? email){
    email = email?.trim();
    return RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email!);
  }

  static String? validateCode(String? value) {
    if (value == null || value == '')
      return '';
    else
      return null;
  }

  static String? validatePass(String? value) {
    if (value!.length == 0)
      return 'Please enter Password';
    else if (value.length < 6 || value.length > 32) {
      return 'Password must not be less than 6 characters long';
    } else
      return null;
  }
}
