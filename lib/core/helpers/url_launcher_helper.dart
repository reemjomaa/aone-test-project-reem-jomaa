import 'dart:io';

import 'package:aone/core/controllers/app_controller.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';



class UrlLauncherHelper{
  static Future<void> openLauncher(String url) async {
    try{
      await launchUrl(Uri.parse(url));
    }catch(e){
      AppController controller = Get.find();
      controller.showToast(Get.context!, message: 'This link is Unavailable');
    }
  }


  static Future<void> openTelegram({
    required String phone,
    LaunchMode mode = LaunchMode.externalApplication,
  }) async {
    String url = 'https://telegram.me/$phone';
    try {
      await launchUrl(
        Uri.parse('$url'),
        mode: mode,
      );
    } catch(e)  {
      throw Exception('openTelegram could not launching url');
    }
  }

  static Future<void> openWhatsApp({
    required String phone,
    LaunchMode mode = LaunchMode.externalApplication,
  }) async {
    final String urlIOS = 'https://wa.me/$phone';
    final String urlAndroid = 'whatsapp://send?phone=$phone';

    final String effectiveURL = Platform.isIOS ? urlIOS : urlAndroid;

    try{
      await launchUrl(Uri.parse(effectiveURL), mode: mode);
    } catch(e) {
      throw Exception('openWhatsApp could not launching url: $effectiveURL');
    }
  }

}