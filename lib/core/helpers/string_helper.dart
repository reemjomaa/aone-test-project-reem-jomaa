
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

extension StringsHelper on String{
  int getNumberFromString(){
    int num = 0;
    if(this.endsWith('+')){
      num = int.tryParse(this.substring(0,this.length-1))!;
    }else{
      num = int.tryParse(this)!;
    }
    return num;
  }

  bool isNumberHasPlus(){
    bool hasPlus = this.endsWith('+');
    return hasPlus;
  }

  LatLng extractLatLngFromURl(){
    final regex = RegExp(r'@(-?\d+\.\d+),(-?\d+\.\d+)');
    final match = regex.firstMatch(this);
    late LatLng location;
    if (match != null) {
      final latitude = double.parse(match.group(1)!);
      final longitude = double.parse(match.group(2)!);
      location = LatLng(latitude, longitude);
    }
    return location;
  }

}