

import 'package:aone/core/style/colors.dart';
import 'package:flutter/material.dart';

abstract class AppGradient {

  static LinearGradient getSplashGradient(){
    return LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: const [0,0.3,0.8],
      colors: AppColors.splashGradientColors,
    );
  }

  static LinearGradient getHomeAppBarGradient(){
    return LinearGradient(
      begin: Alignment.topRight,
      end: Alignment.topLeft,
      tileMode: TileMode.clamp,
      stops: const [0, 0.25,0.6],
      colors: AppColors.homeAppBarGradientColors,
    );
  }

  static LinearGradient getHomeExtraInfoGradient(){
    return LinearGradient(
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
        colors: AppColors.homeExtraInfoGradientColors,
        stops: const [0,0.3,0.8]
    );
  }

}
