
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



abstract class AppColors {
  static const Color primaryColor = Color(0xff20125D);
  static const Color secondaryColor = Color(0xff0EE392);

  static const Color authBackgroundColor = Color(0xff2F1B8E);
  static const Color backgroundColor = Color(0xff0F0734);

  static const Color textPrimaryColor = Color(0xFF00F68C);
  static const Color whiteColor = Color(0xFFFFFFFF);
  static Color whiteOpacityColor(double? opacity) => const Color(0xFFffffff).withOpacity(opacity??1);
  static Color dividerOpacityColor(double? opacity) => const Color(0xFF11D991).withOpacity(opacity??1);
  // static const textPrimaryColor = Color(0xFF0EE392);

  static Color greenishGradientColor(double? opacity) => const Color(0xFF52B398).withOpacity(opacity??1);
  static Color bluishGradientColor(double? opacity) => const Color(0xFF2F1B8E).withOpacity(opacity??1);

  static List<Color> splashGradientColors = [
    greenishGradientColor(0.5),
    bluishGradientColor(0.7),
    backgroundColor.withOpacity(0.7),
  ];

  static List<Color> homeAppBarGradientColors = [
    greenishGradientColor(0.3),
    bluishGradientColor(0.65),
    AppColors.primaryColor.withOpacity(0.8),
  ];

  static List<Color> homeExtraInfoGradientColors = [
    AppColors.primaryColor.withOpacity(0.8),
    AppColors.primaryColor.withOpacity(0.5),
    Colors.transparent,
  ];



}


