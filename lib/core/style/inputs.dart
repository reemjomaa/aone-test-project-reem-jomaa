
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'colors.dart';


abstract class AppInputFieldTheme {

  static InputDecorationTheme mainThemeDecoration() {
    return InputDecorationTheme(
      filled: true,
      focusColor: AppColors.secondaryColor,
      fillColor: AppColors.primaryColor,
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      errorStyle: const TextStyle(fontSize: 13, color: Colors.redAccent,fontWeight: FontWeight.w500),
      hintStyle: const TextStyle(fontSize: 15, color: AppColors.primaryColor,fontWeight: FontWeight.w500),
      disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 1.5,
              color: AppColors.primaryColor
          ),
          borderRadius: BorderRadius.circular(10),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: const BorderSide(
            width: 1.5,
            color: AppColors.secondaryColor
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: const BorderSide(
            width: 1.5,
            color: AppColors.secondaryColor
        ),
        borderRadius: BorderRadius.circular(10)
      ),
      errorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 1.5,
              color: Colors.redAccent
          ),
          borderRadius: BorderRadius.circular(10)
      ),
      focusedErrorBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 1.5,
              color: AppColors.secondaryColor
          ),
          borderRadius: BorderRadius.circular(10)
      ),
    );
  }

  static InputDecorationTheme borderLessDecoration() {
    return InputDecorationTheme(
      focusColor: AppColors.secondaryColor,
      // hintStyle: Get.textTheme.bodyMedium!.copyWith(color: AppColors.secondaryColor),
      hintStyle: Get.theme.textTheme.bodyMedium,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
      disabledBorder: InputBorder.none,
      errorBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      labelStyle: Get.theme.textTheme.bodySmall,
    );
  }


  static InputDecorationTheme labelThemeDecoration() {
    return InputDecorationTheme(
      focusColor: AppColors.secondaryColor,
      errorStyle: const TextStyle(height: 0, color: AppColors.primaryColor),
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 18),
      hintStyle: const TextStyle(fontSize: 15, color: AppColors.primaryColor),
      labelStyle: Get.theme.textTheme.bodySmall!.copyWith(color: AppColors.primaryColor,),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      border: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      disabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      focusedErrorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
    );
  }

  static InputDecorationTheme dropDownDecoration() {
    return InputDecorationTheme(
      focusColor: AppColors.secondaryColor,
      errorStyle: const TextStyle(height: 0, color: AppColors.primaryColor),
      contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 18),
      hintStyle: const TextStyle(fontSize: 15, color: AppColors.secondaryColor),
      labelStyle: Get.theme.textTheme.bodySmall!.copyWith(color: AppColors.primaryColor,),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      border: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      disabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
      focusedErrorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: AppColors.primaryColor),
      ),
    );
  }

}
