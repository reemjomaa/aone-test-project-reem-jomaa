

import 'package:flutter/material.dart';

abstract class AppShadows {
  static List<BoxShadow> boxShadow16 = [
    BoxShadow(
      offset: const Offset(0, 3),
      blurRadius: 6,
      color: Colors.black.withOpacity(0.16),
    ),
  ];
}