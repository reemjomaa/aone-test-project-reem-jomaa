
import 'package:aone/core/style/inputs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../colors.dart';
import '../font_weight.dart';

abstract class AppDarkTheme {
  static ThemeData darkTheme() => ThemeData(
      useMaterial3: true,
      brightness: Brightness.dark,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      scaffoldBackgroundColor: AppColors.backgroundColor,
      dividerTheme: DividerThemeData(
        color: AppColors.dividerOpacityColor(0.5),
        thickness: 1
      ),
      colorScheme: const ColorScheme.light(
        brightness: Brightness.dark,
        primary: AppColors.primaryColor,
        secondary: AppColors.secondaryColor,
        surface: AppColors.whiteColor,
      ),
      switchTheme: SwitchThemeData(
          thumbColor: WidgetStateProperty.resolveWith((states) =>
          states.contains(WidgetState.selected)
              ? AppColors.primaryColor
              : null)),
      fontFamily: 'Roboto',
      textTheme: TextTheme(
        headlineLarge: TextStyle(fontSize: 44.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.black),
        headlineMedium: TextStyle(fontSize: 40.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.black),
        headlineSmall: TextStyle(fontSize: 32.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.medium),
        titleLarge: TextStyle(fontSize: 27.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        titleMedium: TextStyle(fontSize: 24.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        titleSmall: TextStyle(fontSize: 20.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.semiBold),
        bodyLarge: TextStyle(fontSize: 16.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        bodyMedium: TextStyle(fontSize: 14.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        bodySmall: TextStyle(fontSize: 12.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.semiBold),
        labelLarge: TextStyle(fontSize: 11.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        labelMedium: TextStyle(fontSize: 8.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular),
        labelSmall: TextStyle(fontSize: 5.sp, color: AppColors.whiteColor, fontWeight: AppFontWeight.regular,),
      ),
      inputDecorationTheme: AppInputFieldTheme.mainThemeDecoration()
  );
}
