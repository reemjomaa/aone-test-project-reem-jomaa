import 'package:flutter/material.dart';

import 'dark_theme.dart';

enum AppTheme {Light, Dark}

extension AppThemes on AppTheme {
  String get value {
    switch (this) {
      case AppTheme.Light:
        return 'light';
      case AppTheme.Dark:
        return 'dark';
    }
  }

  ThemeData get getTheme {
    switch (this) {
      case AppTheme.Light:
        return AppDarkTheme.darkTheme();
      case AppTheme.Dark:
        return AppDarkTheme.darkTheme();
    }
  }
}
