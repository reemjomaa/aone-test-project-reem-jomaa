
import 'package:flutter/material.dart';

import 'colors.dart';

abstract class AppDecorations {

  static BoxDecoration borderedDecoration(double radius,Color borderColor,Color fillColor,{double borderWidth=1}){
    return BoxDecoration(
      borderRadius: BorderRadius.circular(radius),
      color: fillColor,
      border: Border.all(color: borderColor,width: borderWidth)
    );
  }

  static BoxDecoration circularBorderDecoration(Color borderColor,{double borderWidth=1,Color? fillColor}){
    return BoxDecoration(
      shape: BoxShape.circle,
      color: fillColor??Colors.transparent,
      border: Border.all(color: borderColor,width: borderWidth)
    );
  }


  static BoxDecoration fillDecoration(double radius,Color fillColor){
    return BoxDecoration(
      borderRadius: BorderRadius.circular(radius),
      color: fillColor,
    );
  }

  static BoxDecoration linearDecoration(double radius,Color borderColor){
    return BoxDecoration(
      borderRadius: BorderRadius.circular(radius),
      border: Border.all(color: borderColor),
      gradient: const LinearGradient(
        colors: [
          // AppStyle.borderColor.withOpacity(0.1),
          AppColors.whiteColor,
        ],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
        stops: [0,0.8],
        tileMode: TileMode.clamp,
      ),
    );
  }

  static BoxDecoration oneSideBorderDecoration(Color borderColor, {bool top = false, bool bottom = false, bool left = false, bool right = false, double width = 1}){
    return BoxDecoration(
        border: Border(
          bottom: bottom? BorderSide(color: borderColor,width: width):BorderSide.none,
          top: top? BorderSide(color: borderColor,width: width):BorderSide.none,
          left: left? BorderSide(color: borderColor,width: width):BorderSide.none,
          right: right? BorderSide(color: borderColor,width: width):BorderSide.none,
        ),
    );
  }

  static BoxDecoration oneSideRadiusDecoration(Color color,double radius, {bool topStart = false, bool bottomStart = false, bool topEnd = false, bool bottomEnd = false}){
    return BoxDecoration(
        color: color,
        borderRadius: BorderRadiusDirectional.only(
          topStart: topStart? Radius.circular(radius): Radius.zero,
          bottomStart: bottomStart? Radius.circular(radius): Radius.zero,
          topEnd: topEnd? Radius.circular(radius): Radius.zero,
          bottomEnd: bottomEnd? Radius.circular(radius): Radius.zero
        ),
    );
  }

  static BoxDecoration shadowDecoration(Color color,Color shadowColor,double radius){
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(radius),
      boxShadow: [
        BoxShadow(
          color: shadowColor,
          spreadRadius: 10,
          blurRadius: 10
        )
      ],
    );
  }

}
