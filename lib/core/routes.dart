
import 'package:aone/features/auth/presentaion/onboarding/onboarding_page.dart';
import 'package:aone/features/auth/presentaion/otp/verify_page.dart';
import 'package:aone/features/auth/presentaion/sign_in/sign_in_page.dart';
import 'package:aone/features/auth/presentaion/sign_up/sign_up_page.dart';
import 'package:aone/features/nav_bar_pages/nav_bar/nav_bar_page.dart';
import 'package:aone/features/nav_bar_pages/speakers/speakers_page.dart';
import 'package:aone/features/splash/splash_screen.dart';
import 'package:get/get.dart';



abstract class AppRouting {
  static GetPage unknownRoute = GetPage(name: Pages.login.value, page: () => const SignInPage());
  static List<GetPage<dynamic>> routes() => [
        GetPage(name: Pages.splash.value, page: () => SplashScreen()),
        GetPage(name: Pages.onBoarding.value, page: () => const OnBoardingPage()),
        GetPage(name: Pages.login.value, page: () => const SignInPage()),
        GetPage(name: Pages.signUp.value, page: () => const SignUpPage()),
        GetPage(name: Pages.verify.value, page: () => const VerifyPage()),
        GetPage(name: Pages.speakers.value, page: () => const SpeakersPage()),
        GetPage(name: Pages.navBar.value, page: () => const NavBarPage()),
  ];
}

enum Pages {
  splash,
  onBoarding,
  login,
  signUp,
  verify,
  speakers,
  navBar;

  String get value => '/${this.name}';
}

abstract class Nav {
  static Future? to(Pages page, {dynamic arguments,bool preventDuplicates=true}) =>
      Get.toNamed(page.value, arguments: arguments,preventDuplicates: preventDuplicates);

  static Future? replacement(Pages page, {dynamic arguments}) =>
      Get.offNamed(page.value, arguments: arguments);

  static Future? offAll(Pages page, {dynamic arguments}) =>
      Get.offAllNamed(page.value, arguments: arguments);

  static Future? offNamedUntil(Pages page, {dynamic arguments}) =>
      Get.offNamedUntil(page.value, (route) => false, arguments: arguments);

}
