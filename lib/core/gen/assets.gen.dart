/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class $AssetsGifGen {
  const $AssetsGifGen();

  /// File path: assets/gif/loading.gif
  AssetGenImage get loading => const AssetGenImage('assets/gif/loading.gif');

  /// List of all assets
  List<AssetGenImage> get values => [loading];
}

class $AssetsGlobalGen {
  const $AssetsGlobalGen();

  /// File path: assets/global/app_logo.png
  AssetGenImage get appLogo =>
      const AssetGenImage('assets/global/app_logo.png');

  /// List of all assets
  List<AssetGenImage> get values => [appLogo];
}

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/address_icon.svg
  SvgGenImage get addressIcon =>
      const SvgGenImage('assets/icons/address_icon.svg');

  /// File path: assets/icons/calender_icon.svg
  SvgGenImage get calenderIcon =>
      const SvgGenImage('assets/icons/calender_icon.svg');

  /// File path: assets/icons/home_icon.svg
  SvgGenImage get homeIcon => const SvgGenImage('assets/icons/home_icon.svg');

  /// File path: assets/icons/linkedIn_icon.png
  AssetGenImage get linkedInIcon =>
      const AssetGenImage('assets/icons/linkedIn_icon.png');

  /// File path: assets/icons/more_icon.svg
  SvgGenImage get moreIcon => const SvgGenImage('assets/icons/more_icon.svg');

  /// File path: assets/icons/profile_icon.svg
  SvgGenImage get profileIcon =>
      const SvgGenImage('assets/icons/profile_icon.svg');

  /// File path: assets/icons/time_icon.svg
  SvgGenImage get timeIcon => const SvgGenImage('assets/icons/time_icon.svg');

  /// File path: assets/icons/x_icon.png
  AssetGenImage get xIcon => const AssetGenImage('assets/icons/x_icon.png');

  /// File path: assets/icons/youtube_icon.png
  AssetGenImage get youtubeIcon =>
      const AssetGenImage('assets/icons/youtube_icon.png');

  /// List of all assets
  List<dynamic> get values => [
        addressIcon,
        calenderIcon,
        homeIcon,
        linkedInIcon,
        moreIcon,
        profileIcon,
        timeIcon,
        xIcon,
        youtubeIcon
      ];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/auth_background.png
  AssetGenImage get authBackground =>
      const AssetGenImage('assets/images/auth_background.png');

  /// File path: assets/images/extra_info_background.png
  AssetGenImage get extraInfoBackground =>
      const AssetGenImage('assets/images/extra_info_background.png');

  /// File path: assets/images/home_app_bar_packground.png
  AssetGenImage get homeAppBarPackground =>
      const AssetGenImage('assets/images/home_app_bar_packground.png');

  /// List of all assets
  List<AssetGenImage> get values =>
      [authBackground, extraInfoBackground, homeAppBarPackground];
}

class $AssetsLottieGen {
  const $AssetsLottieGen();

  /// File path: assets/lottie/airplan_loading.json
  LottieGenImage get airplanLoading =>
      const LottieGenImage('assets/lottie/airplan_loading.json');

  /// File path: assets/lottie/error_horizontal.json
  LottieGenImage get errorHorizontal =>
      const LottieGenImage('assets/lottie/error_horizontal.json');

  /// File path: assets/lottie/imageLoading.json
  LottieGenImage get imageLoading =>
      const LottieGenImage('assets/lottie/imageLoading.json');

  /// File path: assets/lottie/loading.json
  LottieGenImage get loading =>
      const LottieGenImage('assets/lottie/loading.json');

  /// File path: assets/lottie/notfound.json
  LottieGenImage get notfound =>
      const LottieGenImage('assets/lottie/notfound.json');

  /// List of all assets
  List<LottieGenImage> get values =>
      [airplanLoading, errorHorizontal, imageLoading, loading, notfound];
}

class Assets {
  Assets._();

  static const $AssetsGifGen gif = $AssetsGifGen();
  static const $AssetsGlobalGen global = $AssetsGlobalGen();
  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsLottieGen lottie = $AssetsLottieGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class LottieGenImage {
  const LottieGenImage(this._assetName);

  final String _assetName;

  LottieBuilder lottie({
    Animation<double>? controller,
    bool? animate,
    FrameRate? frameRate,
    bool? repeat,
    bool? reverse,
    LottieDelegates? delegates,
    LottieOptions? options,
    void Function(LottieComposition)? onLoaded,
    LottieImageProviderFactory? imageProviderFactory,
    Key? key,
    AssetBundle? bundle,
    Widget Function(BuildContext, Widget, LottieComposition?)? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    double? width,
    double? height,
    BoxFit? fit,
    AlignmentGeometry? alignment,
    String? package,
    bool? addRepaintBoundary,
    FilterQuality? filterQuality,
    void Function(String)? onWarning,
  }) {
    return Lottie.asset(
      _assetName,
      controller: controller,
      animate: animate,
      frameRate: frameRate,
      repeat: repeat,
      reverse: reverse,
      delegates: delegates,
      options: options,
      onLoaded: onLoaded,
      imageProviderFactory: imageProviderFactory,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      package: package,
      addRepaintBoundary: addRepaintBoundary,
      filterQuality: filterQuality,
      onWarning: onWarning,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
