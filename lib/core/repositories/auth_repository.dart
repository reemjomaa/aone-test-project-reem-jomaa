import 'package:aone/core/controllers/app_controller.dart';
import 'package:aone/core/controllers/data_controller.dart';
import 'package:aone/core/models/response_model.dart';
import 'package:aone/core/resources/api.dart';
import 'package:get/get.dart';

class AuthRepo {
  static DataController dataController = Get.find<DataController>();
  static AppController appController = Get.find<AppController>();

  static Future<ResponseModel> requestOtp(String email) async {
    return await dataController.postData(
      url: API.requestOtp,
      body: {
        "email": email,
      },
    );
  }

  static Future<ResponseModel> verifyOtp(String email,String otp,{String? extraParam}) async {
    return await dataController.postData(
      url: API.verify,
      body: {
        "email": email,
        "otp": otp,
      },
      extraParam: extraParam
    );
  }

  static Future<ResponseModel> logout() async {
    return await dataController.getData(
      url: API.logout,
    );
  }

}